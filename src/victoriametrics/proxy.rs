use std::time::Duration;

use actix_web::{dev, HttpResponse};
use awc::ClientResponse;

#[derive(Debug, Clone)]
pub struct VictoriaMetricsProxyConfig {
    pub url: String,
    pub username: String,
    pub passwd: String,
}

impl VictoriaMetricsProxyConfig {
    pub fn with(url: String, username: String, passwd: String) -> Self {
        Self {
            url,
            username,
            passwd,
        }
    }
}

#[derive(Debug, Clone)]
pub struct ProxyRequestBuilder {
    params: Vec<(String, String)>,
}

impl Default for ProxyRequestBuilder {
    fn default() -> Self {
        let params = Vec::with_capacity(4);
        Self { params }
    }
}

impl ProxyRequestBuilder {
    pub fn new() -> Self {
        let params = Vec::with_capacity(4);
        Self { params }
    }

    pub fn query(mut self, query: impl Into<String>) -> Self {
        self.params.push(("query".into(), query.into()));
        self
    }

    pub fn label(mut self, label: &str, fingerprint: Option<String>) -> Self {
        let q = match fingerprint {
            Some(f) => format!("{}{{fingerprint='{}'}}", label, f),
            None => label.to_string(),
        };

        self.params.push(("query".into(), q));
        self
    }

    pub fn start(mut self, start: impl Into<String>) -> Self {
        self.params.push(("start".into(), start.into()));
        self
    }

    pub fn end(mut self, end: impl Into<String>) -> Self {
        self.params.push(("end".into(), end.into()));
        self
    }
}

pub struct VictoriaMetricsProxy {
    config: VictoriaMetricsProxyConfig,
    client: awc::Client,
}

impl VictoriaMetricsProxy {
    pub fn with(config: &VictoriaMetricsProxyConfig) -> Self {
        let client = awc::ClientBuilder::new()
            .timeout(Duration::from_secs(60))
            .finish();

        Self {
            config: config.clone(),
            client,
        }
    }

    pub async fn query(&self, params: &Vec<(String, String)>) -> HttpResponse {
        let req = self.client.get(&self.config.url).query(params);
        if req.is_err() {
            return HttpResponse::InternalServerError()
                .body("Encountered error while building request");
        }

        let res = req
            .unwrap()
            .basic_auth(&self.config.username, &self.config.passwd)
            .insert_header(("User-Agent", "awc/3.0"))
            .send()
            .await;

        if let Ok(res) = res {
            return res.into_http_response();
        }

        HttpResponse::InternalServerError().into()
    }

    pub async fn send(&self, proxy_req: ProxyRequestBuilder) -> HttpResponse {
        self.query(&proxy_req.params).await
    }
}

/// Trait for converting a [`ClientResponse`] into a [`HttpResponse`].
///
/// You can implement this trait on your types, of course, but its
/// main goal is to enable [`ClientResponse`] as return value in
/// [`impl Responder`](actix_web::Responder) contexts.
///
/// [`ClientResponse`]: ClientResponse
/// [`HttpResponse`]: HttpResponse
///
pub trait IntoHttpResponse {
    /// Creates a [`HttpResponse`] from `self`.
    ///
    /// [`HttpResponse`]: HttpResponse
    ///
    fn into_http_response(self) -> HttpResponse;

    /// Wraps the [`HttpResponse`] created by [`into_http_response`]
    /// in a `Result`.
    ///
    /// # Errors
    ///
    /// Because [`into_http_response`] is infallible, this method is,
    /// too.
    /// So calling this method never fails and never returns an `Err`.
    ///
    /// [`HttpResponse`]: HttpResponse
    /// [`into_http_response`]: Self::into_http_response
    ///
    fn into_wrapped_http_response<E>(self) -> Result<HttpResponse, E>
    where
        Self: Sized,
    {
        Ok(self.into_http_response())
    }
}

impl IntoHttpResponse for ClientResponse<dev::Decompress<dev::Payload>> {
    fn into_http_response(self) -> HttpResponse {
        let mut response = HttpResponse::build(self.status());

        for header in self.headers() {
            response.append_header(header);
        }

        response.streaming(self)
    }
}
