use std::collections::HashMap;

use apistos::{ApiType, InstanceType, TypedSchema};
use serde::{Deserialize, Serialize};

use super::labels::MetricsLabel;

#[derive(Debug, Default)]
pub struct VictoriaMetricsRefBuilder {
    metric: Option<MetricsLabel>,
    fingerprint: Option<String>,
    start: Option<String>,
    end: Option<String>,
}

// impl Default for VictoriaMetricsRefBuilder {
//     fn default() -> Self {
//         Self {
//             metric: None,
//             fingerprint: None,
//             start: None,
//             end: None,
//         }
//     }
// }

impl VictoriaMetricsRefBuilder {
    pub fn with_metric(mut self, m: MetricsLabel) -> Self {
        self.metric = Some(m);
        self
    }

    pub fn with_fingerprint(mut self, f: String) -> Self {
        self.fingerprint = Some(f);
        self
    }

    pub fn with_start(mut self, s: String) -> Self {
        self.start = Some(s);
        self
    }

    pub fn with_end(mut self, e: String) -> Self {
        self.end = Some(e);
        self
    }

    pub fn build(self) -> Result<VictoriaMetricsRef, String> {
        let mut path = match self.metric {
            Some(ref m) => {
                let path_str: &str = m.into();
                String::from(path_str)
            }
            _ => return Err("cannot build ref without a label".to_string()),
        };

        if let Some(ref fingerprint) = self.fingerprint {
            let filter = format!(r#"fingerprint="{}""#, fingerprint);
            path.push_str(format!("{{{}}}", filter).as_str());
        }

        let path = format!("{}/{}", VM_BASE_URL, path);

        let mut params = HashMap::new();
        if let Some(start) = self.start {
            params.insert("start", start);
        }

        if let Some(end) = self.end {
            params.insert("end", end);
        }

        let params =
            serde_urlencoded::to_string(&params).map_err(|e| e.to_string())?;

        let mut r#ref = path;
        if !params.is_empty() {
            r#ref.push('?');
            r#ref.push_str(params.as_str());
        }

        Ok(VictoriaMetricsRef { r#ref })
    }
}

const VM_BASE_URL: &str = "/api/metrics";

#[derive(Debug, Clone, Serialize, Deserialize, ApiType)]
pub struct VictoriaMetricsRef {
    /// Link to VictoriaMetrics data
    r#ref: String,
}

impl TypedSchema for VictoriaMetricsRef {
    fn schema_type() -> InstanceType {
        InstanceType::String
    }

    fn format() -> Option<String> {
        None
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_metrics_ref() {
        // MetricsLabel::BridgeBandwidth
        let vm_ref1: VictoriaMetricsRef = VictoriaMetricsRefBuilder::default()
            .with_metric(MetricsLabel::BridgeBandwidth)
            .with_end("-1d".into())
            .build()
            .unwrap();

        let vm_ref2: VictoriaMetricsRef = VictoriaMetricsRefBuilder::default()
            .with_metric(MetricsLabel::BridgeBandwidth)
            .with_start("-1d".into())
            .build()
            .unwrap();

        let vm_ref3: VictoriaMetricsRef = VictoriaMetricsRefBuilder::default()
            .with_metric(MetricsLabel::BridgeBandwidth)
            .with_start("-1d".into())
            .with_end("-30d".into())
            .build()
            .unwrap();

        let vm_ref4: VictoriaMetricsRef = VictoriaMetricsRefBuilder::default()
            .with_metric(MetricsLabel::BridgeBandwidth)
            .build()
            .unwrap();

        assert_eq!(
            vm_ref1.r#ref.as_str(),
            "/api/metrics/bridge_bandwidth?end=-1d"
        );
        assert_eq!(
            vm_ref2.r#ref.as_str(),
            "/api/metrics/bridge_bandwidth?start=-1d"
        );
        assert!(
            vm_ref3.r#ref.as_str()
                == "/api/metrics/bridge_bandwidth?start=-1d&end=-30d"
                || vm_ref3.r#ref.as_str()
                    == "/api/metrics/bridge_bandwidth?end=-30d&start=-1d"
        );
        assert_eq!(vm_ref4.r#ref.as_str(), "/api/metrics/bridge_bandwidth");
    }

    #[test]
    fn test_metrics_fingerprint_filtering_ref() {
        let vm_ref1: VictoriaMetricsRef = VictoriaMetricsRefBuilder::default()
            .with_metric(MetricsLabel::BridgeBandwidth)
            .with_fingerprint("$abcsefgad2222".into())
            .build()
            .unwrap();

        assert_eq!(
            vm_ref1.r#ref.as_str(),
            r#"/api/metrics/bridge_bandwidth{fingerprint="$abcsefgad2222"}"#
        );
    }
}
