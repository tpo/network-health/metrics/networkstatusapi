use sea_query::{Expr, PostgresQueryBuilder, Query, SelectStatement};
use sea_query_binder::{SqlxBinder, SqlxValues};
use sqlx::PgPool;

use super::valid_servers_with_clause;
use crate::{
    metrics::tables::ServerStatus,
    models::query::{domain::ParametersType, params::QueryFilters},
};

fn summary_query_select(
    filters: &QueryFilters,
) -> Result<SelectStatement, String> {
    if filters.search.is_some() {
        return Err("search query params is not supported yet.".to_string());
    }

    if filters.os.is_some() {
        return Err("os query params is not supported yet.".to_string());
    }

    if filters.host_name.is_some() {
        return Err("host_name query params is not supported yet.".to_string());
    }

    if filters.fields.is_some() {
        return Err("fields query params is not supported yet.".to_string());
    }

    if filters.contact.is_some() {
        return Err("contact query params is not supported yet.".to_string());
    }

    if filters.first_seen_days.is_some() {
        return Err(
            "first_seen_days query params is not supported yet.".to_string()
        );
    }

    if filters.last_seen_days.is_some() {
        return Err(
            "last_seen_days query params is not supported yet.".to_string()
        );
    }

    if filters.family.is_some() {
        return Err("family query params is not supported yet.".to_string());
    }

    if filters.lookup.is_some() {
        return Err("lookup query params is not supported yet.".to_string());
    }

    let mut q = Query::select()
        .from(ServerStatus::Table)
        .columns([
            ServerStatus::Nickname,
            ServerStatus::Fingerprint,
            ServerStatus::OrAddresses,
            ServerStatus::Running,
        ])
        .to_owned();

    let is_bridge = match filters.r#type.unwrap_or(ParametersType::Relay) {
        ParametersType::Relay => false,
        ParametersType::Bridge => true,
    };

    q.and_where(Expr::col(ServerStatus::IsBridge).eq(is_bridge));

    if let Some(ref r#as) = filters.r#as {
        q.and_where(Expr::col(ServerStatus::AutonomousSystem).eq(r#as));
    }

    if let Some(ref fs) = filters.first_seen_since {
        let timestamp_stmt = format!("to_timestamp('{}', 'YYYY-MM-DD')", fs);
        q.and_where(Expr::col(ServerStatus::FirstSeen).gte(timestamp_stmt));
    }

    if let Some(ref ls) = filters.last_seen_since {
        let timestamp_stmt = format!("to_timestamp('{}', 'YYYY-MM-DD')", ls);
        q.and_where(Expr::col(ServerStatus::LastSeen).gte(timestamp_stmt));
    }

    if let Some(running) = filters.running {
        q.and_where(Expr::col(ServerStatus::Running).eq(running));
    }

    if let Some(ref country) = filters.country {
        q.and_where(Expr::col(ServerStatus::Country).eq(country.as_ref()));
    }

    if let Some(ref as_name) = filters.as_name {
        q.and_where(Expr::col(ServerStatus::AsName).eq(as_name));
    }

    if let Some(limit) = filters.limit {
        let limit = limit.as_ref().to_owned() as u64;
        q.limit(limit);
    }

    Ok(q)
}

#[derive(Debug, sqlx::FromRow)]
pub struct RelaySummaryRow {
    pub nickname: String,
    pub fingerprint: String,
    pub or_addresses: String,
    pub running: Option<bool>,
}

pub async fn relay_summary(
    pg: &PgPool,
    filters: &QueryFilters,
) -> Result<Vec<RelaySummaryRow>, String> {
    let with = valid_servers_with_clause(None, None);
    let (sql, values) = summary_query_select(filters)?
        .with(with)
        .build_sqlx(PostgresQueryBuilder);
    sqlx::query_as_with::<_, RelaySummaryRow, SqlxValues>(&sql, values)
        .fetch_all(pg)
        .await
        .map_err(|e| e.to_string())
}

#[derive(Debug, sqlx::FromRow)]
pub struct BridgeSummaryRow {
    pub nickname: String,
    pub fingerprint: String,
    pub running: Option<bool>,
}

pub async fn bridges_summary(
    pg: &PgPool,
    filters: &QueryFilters,
) -> Result<Vec<BridgeSummaryRow>, String> {
    let with = valid_servers_with_clause(None, None);
    let (sql, values) = summary_query_select(filters)?
        .with(with)
        .build_sqlx(PostgresQueryBuilder);
    sqlx::query_as_with::<_, BridgeSummaryRow, _>(&sql, values)
        .fetch_all(pg)
        .await
        .map_err(|e| e.to_string())
}
