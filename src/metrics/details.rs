use sea_query::{Expr, PostgresQueryBuilder, Query};
use sea_query_binder::SqlxBinder;
use sqlx::{types::chrono, PgPool};

use super::valid_servers_with_clause;
use crate::{
    metrics::tables::ServerStatus, models::query::params::QueryFilters,
};

#[derive(Debug, sqlx::FromRow, Clone)]
pub struct RelayDetailsRow {
    pub nickname: String,
    pub fingerprint: String,
    pub or_addresses: Option<String>,
    pub last_seen: chrono::NaiveDateTime,
    pub first_seen: chrono::NaiveDateTime,
    pub running: Option<bool>,
    pub flags: Option<String>,
    pub country: Option<String>,
    pub country_name: Option<String>,
    pub autonomous_system: Option<String>,
    pub as_name: Option<String>,
    pub verified_host_names: Option<String>,
    pub last_restarted: chrono::NaiveDateTime,
    pub exit_policy: Option<String>,
    pub contact: Option<String>,
    pub platform: Option<String>,
    pub version: Option<String>,
    pub version_status: Option<String>,
    pub effective_family: Option<String>,
    pub declared_family: Option<String>,
    pub last_changed_address_or_port: chrono::NaiveDateTime,
    pub diff_or_addresses: Option<String>,
    pub unverified_host_names: Option<String>,
    pub unreachable_or_addresses: Option<String>,
    pub overload_ratelimits_timestamp: Option<chrono::NaiveDateTime>,
    pub overload_ratelimits_ratelimit: Option<i64>,
    pub overload_ratelimits_burstlimit: Option<i64>,
    pub overload_ratelimits_read_count: Option<i64>,
    pub overload_ratelimits_write_count: Option<i64>,
    pub overload_fd_exhausted_timestamp: Option<chrono::NaiveDateTime>,
    pub overload_general_timestamp: Option<chrono::NaiveDateTime>,
    pub family_digest: Option<String>,
    pub advertised_bandwidth: Option<i64>,
    pub bandwidth: Option<i64>,
    pub network_weight_fraction: Option<f64>,
    pub bandwidth_avg: Option<i64>,
    pub bandwidth_burst: Option<i64>,
    pub bandwidth_observed: Option<i64>,
    pub ipv6_default_policy: Option<String>,
    pub dir_port: Option<i32>,
    pub is_hibernating: Option<bool>,
}

pub async fn relay_details(
    pg: &PgPool,
    filters: &QueryFilters,
) -> Result<Vec<RelayDetailsRow>, String> {
    if filters.search.is_some() {
        return Err("search query params is not supported yet.".to_string());
    }

    if filters.os.is_some() {
        return Err("os query params is not supported yet.".to_string());
    }

    if filters.host_name.is_some() {
        return Err("host_name query params is not supported yet.".to_string());
    }

    if filters.fields.is_some() {
        return Err("fields query params is not supported yet.".to_string());
    }

    if filters.contact.is_some() {
        return Err("contact query params is not supported yet.".to_string());
    }

    if filters.first_seen_days.is_some() {
        return Err(
            "first_seen_days query params is not supported yet.".to_string()
        );
    }

    if filters.last_seen_days.is_some() {
        return Err(
            "last_seen_days query params is not supported yet.".to_string()
        );
    }

    if filters.family.is_some() {
        return Err("family query params is not supported yet.".to_string());
    }

    if filters.lookup.is_some() {
        return Err("lookup query params is not supported yet.".to_string());
    }

    // Assuming `filters.published` is an Option<String>
    let with = if let Some(published) = filters.published.as_ref() {
        // Assuming the published date is correctly formatted
        valid_servers_with_clause(Some(published), Some("1 week"))
    } else {
        // Call without specific parameters, will use defaults
        valid_servers_with_clause(None, None)
    };

    let mut q = Query::select()
        .from(ServerStatus::Table)
        .columns([
            (ServerStatus::Table, ServerStatus::Nickname),
            (ServerStatus::Table, ServerStatus::Fingerprint),
            (ServerStatus::Table, ServerStatus::OrAddresses),
            (ServerStatus::Table, ServerStatus::LastSeen),
            (ServerStatus::Table, ServerStatus::FirstSeen),
            (ServerStatus::Table, ServerStatus::Running),
            (ServerStatus::Table, ServerStatus::Flags),
            (ServerStatus::Table, ServerStatus::Country),
            (ServerStatus::Table, ServerStatus::CountryName),
            (ServerStatus::Table, ServerStatus::AutonomousSystem),
            (ServerStatus::Table, ServerStatus::AsName),
            (ServerStatus::Table, ServerStatus::VerifiedHostNames),
            (ServerStatus::Table, ServerStatus::LastRestarted),
            (ServerStatus::Table, ServerStatus::ExitPolicy),
            (ServerStatus::Table, ServerStatus::Contact),
            (ServerStatus::Table, ServerStatus::Platform),
            (ServerStatus::Table, ServerStatus::Version),
            (ServerStatus::Table, ServerStatus::VersionStatus),
            (ServerStatus::Table, ServerStatus::EffectiveFamily),
            (ServerStatus::Table, ServerStatus::DeclaredFamily),
            (ServerStatus::Table, ServerStatus::LastChangedAddressOrPort),
            (ServerStatus::Table, ServerStatus::DiffOrAddresses),
            (ServerStatus::Table, ServerStatus::UnverifiedHostNames),
            (ServerStatus::Table, ServerStatus::UnreachableOrAddresses),
            (
                ServerStatus::Table,
                ServerStatus::OverloadRatelimitsTimestamp,
            ),
            (
                ServerStatus::Table,
                ServerStatus::OverloadRatelimitsRatelimit,
            ),
            (
                ServerStatus::Table,
                ServerStatus::OverloadRatelimitsBurstlimit,
            ),
            (
                ServerStatus::Table,
                ServerStatus::OverloadRatelimitsReadCount,
            ),
            (
                ServerStatus::Table,
                ServerStatus::OverloadRatelimitsWriteCount,
            ),
            (
                ServerStatus::Table,
                ServerStatus::OverloadFdExhaustedTimestamp,
            ),
            (ServerStatus::Table, ServerStatus::OverloadGeneralTimestamp),
            (ServerStatus::Table, ServerStatus::FamilyDigest),
            (ServerStatus::Table, ServerStatus::AdvertisedBandwidth),
            (ServerStatus::Table, ServerStatus::Bandwidth),
            (ServerStatus::Table, ServerStatus::NetworkWeightFraction),
            (ServerStatus::Table, ServerStatus::BandwidthAvg),
            (ServerStatus::Table, ServerStatus::BandwidthBurst),
            (ServerStatus::Table, ServerStatus::BandwidthObserved),
            (ServerStatus::Table, ServerStatus::Ipv6DefaultPolicy),
            (ServerStatus::Table, ServerStatus::SocksPort),
            (ServerStatus::Table, ServerStatus::DirPort),
            (ServerStatus::Table, ServerStatus::OrPort),
            (ServerStatus::Table, ServerStatus::IsHibernating),
        ])
        .to_owned();

    q.and_where(
        Expr::col((ServerStatus::Table, ServerStatus::IsBridge)).eq(false),
    );

    if let Some(ref published) = filters.published {
        let timestamp = published;
        q.and_where(
            Expr::col((ServerStatus::Table, ServerStatus::Published))
                .gte(Expr::cust(format!(
                    "TIMESTAMP '{}' - INTERVAL '{}'",
                    timestamp, "1 week"
                )))
                .and(
                    Expr::col((ServerStatus::Table, ServerStatus::Published))
                        .lte(Expr::cust(format!(
                            "TIMESTAMP '{}' + INTERVAL '{}'",
                            timestamp, "1 week"
                        ))),
                ),
        );
    }

    if let Some(ref r#as) = filters.r#as {
        q.and_where(
            Expr::col((ServerStatus::Table, ServerStatus::AutonomousSystem))
                .eq(r#as),
        );
    }

    if let Some(ref fs) = filters.first_seen_since {
        let timestamp_stmt = format!("to_timestamp('{}', 'YYYY-MM-DD')", fs);
        q.and_where(
            Expr::col((ServerStatus::Table, ServerStatus::FirstSeen))
                .gte(timestamp_stmt),
        );
    }

    if let Some(ref ls) = filters.last_seen_since {
        let timestamp_stmt = format!("to_timestamp('{}', 'YYYY-MM-DD')", ls);
        q.and_where(
            Expr::col((ServerStatus::Table, ServerStatus::LastSeen))
                .gte(timestamp_stmt),
        );
    }

    if let Some(running) = filters.running {
        q.and_where(
            Expr::col((ServerStatus::Table, ServerStatus::Running))
                .eq(running),
        );
    }

    if let Some(ref country) = filters.country {
        q.and_where(
            Expr::col((ServerStatus::Table, ServerStatus::Country))
                .eq(country.as_ref()),
        );
    }

    if let Some(ref as_name) = filters.as_name {
        q.and_where(
            Expr::col((ServerStatus::Table, ServerStatus::AsName)).eq(as_name),
        );
    }

    if let Some(limit) = filters.limit {
        let limit = limit.as_ref().to_owned() as u64;
        q.limit(limit);
    } else {
        let limit = 10;
        q.limit(limit);
    }

    let (sql, values) = q.with(with).build_sqlx(PostgresQueryBuilder);

    let result = sqlx::query_as_with::<_, RelayDetailsRow, _>(&sql, values)
        .fetch_all(pg)
        .await
        .map_err(|e| format!("Error in function 'relay_details': {}", e));

    match result {
        Ok(rows) => Ok(rows),
        Err(e) => {
            println!("{}", e); // Optionally log the error
            Err(e)
        }
    }
}

#[derive(Debug, sqlx::FromRow, Clone)]
pub struct BridgeDetailsRow {
    pub nickname: String,
    pub fingerprint: String,
    pub or_addresses: Option<String>,
    pub first_seen: chrono::NaiveDateTime,
    pub last_seen: chrono::NaiveDateTime,
    pub running: Option<bool>,
    pub flags: Option<String>,
    pub last_restarted: chrono::NaiveDateTime,
    pub overload_general_timestamp: Option<chrono::NaiveDateTime>,
    pub version: Option<String>,
    pub platform: Option<String>,
    pub version_status: Option<String>,
    pub transport: Option<String>,
    pub blocklist: Option<String>,
    pub bridgedb_distributor: Option<String>,
}

pub async fn bridge_details(
    pg: &PgPool,
    filters: &QueryFilters,
) -> Result<Vec<BridgeDetailsRow>, String> {
    if filters.search.is_some() {
        return Err("search query param is not supported yet.".to_string());
    }

    if filters.os.is_some() {
        return Err("os query param is not supported yet.".to_string());
    }

    if filters.host_name.is_some() {
        return Err("host_name query param is not supported yet.".to_string());
    }

    if filters.fields.is_some() {
        return Err("fields query param is not supported yet.".to_string());
    }

    if filters.contact.is_some() {
        return Err("contact query param is not supported yet.".to_string());
    }

    if filters.first_seen_days.is_some() {
        return Err(
            "first_seen_days query param is not supported yet.".to_string()
        );
    }

    if filters.last_seen_days.is_some() {
        return Err(
            "last_seen_days query param is not supported yet.".to_string()
        );
    }

    if filters.family.is_some() {
        return Err("family query params is not supported yet.".to_string());
    }

    if filters.lookup.is_some() {
        return Err("lookup query params is not supported yet.".to_string());
    }

    // Assuming `filters.published` is an Option<String>
    let with = if let Some(ref published) = filters.published {
        // Assuming the published date is correctly formatted
        valid_servers_with_clause(Some(published), Some("1 week"))
    } else {
        // Call without specific parameters, will use defaults
        valid_servers_with_clause(None, None)
    };

    let mut q = Query::select()
        .from(ServerStatus::Table)
        .columns([
            (ServerStatus::Table, ServerStatus::Nickname),
            (ServerStatus::Table, ServerStatus::Fingerprint),
            (ServerStatus::Table, ServerStatus::OrAddresses),
            (ServerStatus::Table, ServerStatus::LastSeen),
            (ServerStatus::Table, ServerStatus::FirstSeen),
            (ServerStatus::Table, ServerStatus::Running),
            (ServerStatus::Table, ServerStatus::Version),
            (ServerStatus::Table, ServerStatus::Platform),
            (ServerStatus::Table, ServerStatus::VersionStatus),
            (ServerStatus::Table, ServerStatus::Transport),
            (ServerStatus::Table, ServerStatus::Blocklist),
            (ServerStatus::Table, ServerStatus::BridgedbDistributor),
            (ServerStatus::Table, ServerStatus::Flags),
            (ServerStatus::Table, ServerStatus::LastRestarted),
            (ServerStatus::Table, ServerStatus::Country),
            (ServerStatus::Table, ServerStatus::CountryName),
            (ServerStatus::Table, ServerStatus::OverloadGeneralTimestamp),
        ])
        .to_owned();

    q.and_where(
        Expr::col((ServerStatus::Table, ServerStatus::IsBridge)).eq(true),
    );

    if let Some(ref published) = filters.published {
        let timestamp = published;
        q.and_where(
            Expr::col((ServerStatus::Table, ServerStatus::Published))
                .gte(Expr::cust(format!(
                    "TIMESTAMP '{}' - INTERVAL '{}'",
                    timestamp, "1 week"
                )))
                .and(
                    Expr::col((ServerStatus::Table, ServerStatus::Published))
                        .lte(Expr::cust(format!(
                            "TIMESTAMP '{}' + INTERVAL '{}'",
                            timestamp, "1 week"
                        ))),
                ),
        );
    }

    if let Some(ref r#as) = filters.r#as {
        q.and_where(
            Expr::col((ServerStatus::Table, ServerStatus::AutonomousSystem))
                .eq(r#as),
        );
    }

    if let Some(ref fs) = filters.first_seen_since {
        let timestamp_stmt = format!("to_timestamp('{}', 'YYYY-MM-DD')", fs);
        q.and_where(
            Expr::col((ServerStatus::Table, ServerStatus::FirstSeen))
                .gte(timestamp_stmt),
        );
    }

    if let Some(ref ls) = filters.last_seen_since {
        let timestamp_stmt = format!("to_timestamp('{}', 'YYYY-MM-DD')", ls);
        q.and_where(
            Expr::col((ServerStatus::Table, ServerStatus::LastSeen))
                .gte(timestamp_stmt),
        );
    }

    if let Some(running) = filters.running {
        q.and_where(
            Expr::col((ServerStatus::Table, ServerStatus::Running))
                .eq(running),
        );
    }

    if let Some(ref country) = filters.country {
        q.and_where(
            Expr::col((ServerStatus::Table, ServerStatus::Country))
                .eq(country.as_ref()),
        );
    }

    if let Some(ref as_name) = filters.as_name {
        q.and_where(
            Expr::col((ServerStatus::Table, ServerStatus::AsName)).eq(as_name),
        );
    }

    if let Some(limit) = filters.limit {
        let limit = limit.as_ref().to_owned() as u64;
        q.limit(limit);
    } else {
        let limit = 10;
        q.limit(limit);
    }

    let (sql, values) = q.with(with).build_sqlx(PostgresQueryBuilder);

    let result = sqlx::query_as_with::<_, BridgeDetailsRow, _>(&sql, values)
        .fetch_all(pg)
        .await
        .map_err(|e| format!("Error in function 'bridge_details': {}", e));

    match result {
        Ok(rows) => Ok(rows),
        Err(e) => {
            println!("{}", e); // Optionally log the error
            Err(e)
        }
    }
}
