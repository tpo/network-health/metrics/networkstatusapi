mod bandwidth;
mod details;
mod exit_node;
mod summary;
mod tables;
mod weights_probability;

pub use bandwidth::*;
use chrono::{DateTime, Local, SecondsFormat};
pub use details::*;
pub use exit_node::*;
use sea_query::*;
use sqlx::{postgres::PgRow, types::chrono::NaiveDateTime, PgPool, Row};
pub use summary::*;
pub use weights_probability::*;

use self::tables::ServerStatus;
use crate::models::query::params::QueryFilters;

fn valid_servers_with_clause(
    custom_timestamp: Option<&str>,
    interval: Option<&str>,
) -> WithClause {
    // Get the current timestamp as a formatted string if none is provided
    let current_timestamp: String =
        Local::now().to_rfc3339_opts(SecondsFormat::Secs, true);

    // Use the provided timestamp or the current timestamp if none is provided
    let timestamp = custom_timestamp.unwrap_or(&current_timestamp);
    let interval = interval.unwrap_or("1 week");

    let inner = SelectStatement::new()
        .from(ServerStatus::Table)
        .distinct_on([ServerStatus::Fingerprint])
        .column(Asterisk)
        .and_where(
            Expr::col((ServerStatus::Table, ServerStatus::Published))
                .gte(Expr::cust(format!(
                    "TIMESTAMP '{}' - INTERVAL '{}'",
                    timestamp, interval
                )))
                .and(
                    Expr::col((ServerStatus::Table, ServerStatus::Published))
                        .lte(Expr::cust(format!(
                            "TIMESTAMP '{}' + INTERVAL '{}'",
                            timestamp, interval
                        ))),
                ),
        )
        .order_by(ServerStatus::Fingerprint, Order::Asc)
        .order_by(ServerStatus::Published, Order::Desc)
        .to_owned();

    let cte = CommonTableExpression::new()
        .query(inner)
        .table_name(Alias::new("server_status"))
        .to_owned();

    WithClause::new().cte(cte).to_owned()
}

pub async fn relays_published(
    pg: &PgPool,
    filters: &QueryFilters,
) -> Result<String, String> {
    let timestamp: String =
        if let Some(published_timestamp) = &filters.published {
            // Clone the string to ensure ownership if needed
            published_timestamp.clone()
        } else {
            Local::now().to_rfc3339_opts(SecondsFormat::Secs, true)
        };

    let sql = Query::select()
        .from(ServerStatus::Table)
        .expr(
            Expr::col((ServerStatus::Table, ServerStatus::Published))
                .gte(Expr::cust(format!(
                    "TIMESTAMP '{}' - INTERVAL '1 week'",
                    timestamp
                )))
                .and(
                    Expr::col((ServerStatus::Table, ServerStatus::Published))
                        .lte(Expr::cust(format!(
                            "TIMESTAMP '{}' + INTERVAL '1 week'",
                            timestamp
                        ))),
                ),
        )
        .and_where(Expr::col(ServerStatus::IsBridge).eq(false))
        .to_string(PostgresQueryBuilder);

    let row = sqlx::query(&sql)
        .map(|row: PgRow| row.get::<NaiveDateTime, _>(0))
        .fetch_one(pg)
        .await
        .map_err(|e| format!("Error in function 'relay_published': {}", e))?;

    Ok(row.to_string())
}

pub async fn bridges_published(
    pg: &PgPool,
    filters: &QueryFilters,
) -> Result<String, String> {
    let timestamp: String =
        if let Some(published_timestamp) = &filters.published {
            // Clone the string to ensure ownership if needed
            published_timestamp.clone()
        } else {
            Local::now().to_rfc3339_opts(SecondsFormat::Secs, true)
        };

    let sql = Query::select()
        .from(ServerStatus::Table)
        .expr(
            Expr::col((ServerStatus::Table, ServerStatus::Published))
                .gte(Expr::cust(format!(
                    "TIMESTAMP '{}' - INTERVAL '1 week'",
                    timestamp
                )))
                .and(
                    Expr::col((ServerStatus::Table, ServerStatus::Published))
                        .lte(Expr::cust(format!(
                            "TIMESTAMP '{}' + INTERVAL '1 week'",
                            timestamp
                        ))),
                ),
        )
        .and_where(Expr::col(ServerStatus::IsBridge).eq(true))
        .to_string(PostgresQueryBuilder);

    let row = sqlx::query(&sql)
        .map(|row: PgRow| row.get::<NaiveDateTime, _>(0))
        .fetch_one(pg)
        .await
        .map_err(|e| {
            format!("Error in function 'bridges_published': {}", e)
        })?;

    Ok(row.to_string())
}

pub async fn published(
    pg: &PgPool,
    filters: &QueryFilters,
) -> Result<(String, String), String> {
    let timestamp: String =
        if let Some(published_timestamp) = &filters.published {
            // Clone the string to ensure ownership if needed
            published_timestamp.clone()
        } else {
            Local::now().to_rfc3339_opts(SecondsFormat::Secs, true)
        };

    let query = format!(
        "WITH ss AS (
            SELECT DISTINCT ON (fingerprint) *
            FROM server_status
            WHERE published >= TIMESTAMP '{timestamp}' - INTERVAL '1 week'
            AND published <= TIMESTAMP '{timestamp}' + INTERVAL '1 week'
            ORDER BY fingerprint, published DESC, is_bridge ASC
        )
        SELECT
            MAX(CASE WHEN is_bridge = true THEN published ELSE NULL END)
                AS max_published_bridge,
            MAX(CASE WHEN is_bridge = false THEN published ELSE NULL END)
                AS max_published_relay
        FROM ss;",
        timestamp = timestamp
    );
    let result = sqlx::query(&query).fetch_one(pg).await;
    match result {
        Ok(row) => {
            // Assuming row.get can fetch and convert to DateTime, which needs
            // to be formatted to String
            let max_published_bridge = row
                .try_get(0)
                // If there date is null or can't be parsed, set unix epoch,
                // which can be unwrapped, since it should be correct
                .unwrap_or_else(|_| DateTime::from_timestamp(0, 0).unwrap());
            let max_published_relay = row
                .try_get(1)
                .unwrap_or_else(|_| DateTime::from_timestamp(0, 0).unwrap());
            Ok((
                // No need to unwrap the date here, since it was already
                // unwrapped in the previous lines.
                max_published_bridge.to_string(),
                max_published_relay.to_string(),
            ))
        }
        Err(e) => Err(format!(
            "Error in function 'metrics::mod::published': {}",
            e
        )),
    }
}

pub async fn totals(
    pg: &PgPool,
    filters: &QueryFilters,
) -> Result<(i64, i64), String> {
    let timestamp: String =
        if let Some(published_timestamp) = &filters.published {
            // Clone the string to ensure ownership if needed
            published_timestamp.clone()
        } else {
            Local::now().to_rfc3339_opts(SecondsFormat::Secs, true)
        };

    let query = format!(
        "WITH ss AS (
            SELECT DISTINCT ON (fingerprint) *
            FROM server_status
            WHERE published >= TIMESTAMP '{timestamp}' - INTERVAL '1 week'
            AND published <= TIMESTAMP '{timestamp}' + INTERVAL '1 week'
            ORDER BY fingerprint, published DESC, is_bridge
        )
        SELECT
            COUNT(*) FILTER (WHERE is_bridge = true) AS bridge_count,
            COUNT(*) FILTER (WHERE is_bridge = false) AS relay_count
        FROM ss;",
        timestamp = timestamp
    );

    let result = sqlx::query_as::<_, (i64, i64)>(&query).fetch_one(pg).await;

    match result {
        Ok((bridge_count, relay_count)) => Ok((bridge_count, relay_count)),
        Err(e) => {
            Err(format!("Error in function 'metrics::mod::totals': {}", e))
        }
    }
}
