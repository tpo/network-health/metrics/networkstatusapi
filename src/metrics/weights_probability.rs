use sea_query::{Expr, PostgresQueryBuilder, Query};
use sea_query_binder::SqlxBinder;
use sqlx::{
    types::{chrono, Uuid},
    PgPool,
};

use super::tables::{NetworkStatusEntry, NetworkStatusEntryWeights};
use crate::models::query::params::QueryFilters;

#[allow(clippy::await_holding_lock)]
#[derive(Debug, Clone, sqlx::FromRow)]
pub struct WeightsProbabilityRow {
    pub nickname: String,
    pub fingerprint: String,
    pub guard_weight_fraction: f64,
    pub middle_weight_fraction: f64,
    pub exit_weight_fraction: f64,
    pub time: chrono::NaiveDateTime,
    pub weights_id: Uuid,
}

pub async fn weights_probability(
    pg: &PgPool,
    filters: &QueryFilters,
) -> Result<Vec<WeightsProbabilityRow>, String> {
    if filters.search.is_some() {
        return Err("search query params is not supported yet.".to_string());
    }

    if filters.os.is_some() {
        return Err("os query params is not supported yet.".to_string());
    }

    if filters.host_name.is_some() {
        return Err("host_name query params is not supported yet.".to_string());
    }

    if filters.fields.is_some() {
        return Err("fields query params is not supported yet.".to_string());
    }

    if filters.contact.is_some() {
        return Err("contact query params is not supported yet.".to_string());
    }

    if filters.first_seen_days.is_some() {
        return Err(
            "first_seen_days query params is not supported yet.".to_string()
        );
    }

    if filters.last_seen_days.is_some() {
        return Err(
            "last_seen_days query params is not supported yet.".to_string()
        );
    }

    if filters.family.is_some() {
        return Err("family query params is not supported yet.".to_string());
    }

    if filters.lookup.is_some() {
        return Err("lookup query params is not supported yet.".to_string());
    }

    if filters.r#type.is_some() {
        return Err("type query params is not supported yet.".to_string());
    }

    if filters.r#as.is_some() {
        return Err("as query param is not supported yet.".to_string());
    }

    if filters.as_name.is_some() {
        return Err("as_name query param is not supported yet.".to_string());
    }

    if filters.first_seen_since.is_some() {
        return Err("first_seen_since param is not supported yet.".to_string());
    }

    if filters.last_seen_since.is_some() {
        return Err(
            "last_seensince query param is not supported yet.".to_string()
        );
    }

    if filters.running.is_some() {
        return Err("running query param is not supported yet.".to_string());
    }

    if filters.country.is_some() {
        return Err("country query param is not supported yet.".to_string());
    }

    let mut q = Query::select()
        .columns([
            (NetworkStatusEntry::Table, NetworkStatusEntry::Nickname),
            (NetworkStatusEntry::Table, NetworkStatusEntry::Fingerprint),
        ])
        .columns([
            (
                NetworkStatusEntryWeights::Table,
                NetworkStatusEntryWeights::GuardWeightFraction,
            ),
            (
                NetworkStatusEntryWeights::Table,
                NetworkStatusEntryWeights::MiddleWeightFraction,
            ),
            (
                NetworkStatusEntryWeights::Table,
                NetworkStatusEntryWeights::ExitWeightFraction,
            ),
            (
                NetworkStatusEntryWeights::Table,
                NetworkStatusEntryWeights::Time,
            ),
            (
                NetworkStatusEntryWeights::Table,
                NetworkStatusEntryWeights::WeightsId,
            ),
        ])
        .from(NetworkStatusEntry::Table)
        .left_join(
            NetworkStatusEntryWeights::Table,
            Expr::col((NetworkStatusEntry::Table, NetworkStatusEntry::Digest))
                .equals((
                    NetworkStatusEntryWeights::Table,
                    NetworkStatusEntryWeights::NetworkStatusEntry,
                )),
        )
        .to_owned();

    if let Some(limit) = filters.limit {
        let limit = limit.as_ref().to_owned() as u64;
        q.limit(limit);
    }

    let (sql, values) = q.build_sqlx(PostgresQueryBuilder);
    sqlx::query_as_with::<_, WeightsProbabilityRow, _>(&sql, values)
        .fetch_all(pg)
        .await
        .map_err(|e| e.to_string())
}
