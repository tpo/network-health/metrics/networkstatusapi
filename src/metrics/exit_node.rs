use sea_query::{Expr, PostgresQueryBuilder, Query};
use sea_query_binder::SqlxBinder;
use sqlx::{types::chrono::NaiveDateTime, PgPool};

use super::{
    tables::{ExitNode, ServerStatus},
    valid_servers_with_clause,
};
use crate::models::query::{domain::ParametersType, params::QueryFilters};

#[derive(Debug, Clone, sqlx::FromRow)]
pub struct ExitNodeRow {
    pub nickname: String,
    pub fingerprint: String,
    pub exit_addresses: String,
    pub published: NaiveDateTime,
}

pub async fn exit_addresses(
    pg: &PgPool,
    filters: &QueryFilters,
) -> Result<Vec<ExitNodeRow>, String> {
    if filters.search.is_some() {
        return Err("search query params is not supported yet.".to_string());
    }

    if filters.os.is_some() {
        return Err("os query params is not supported yet.".to_string());
    }

    if filters.host_name.is_some() {
        return Err("host_name query params is not supported yet.".to_string());
    }

    if filters.fields.is_some() {
        return Err("fields query params is not supported yet.".to_string());
    }

    if filters.contact.is_some() {
        return Err("contact query params is not supported yet.".to_string());
    }

    if filters.first_seen_days.is_some() {
        return Err(
            "first_seen_days query params is not supported yet.".to_string()
        );
    }

    if filters.last_seen_days.is_some() {
        return Err(
            "last_seen_days query params is not supported yet.".to_string()
        );
    }

    if filters.family.is_some() {
        return Err("family query params is not supported yet.".to_string());
    }

    if filters.lookup.is_some() {
        return Err("lookup query params is not supported yet.".to_string());
    }

    let with = valid_servers_with_clause(None, None);

    let mut q = Query::select()
        .from(ServerStatus::Table)
        .columns([
            (ServerStatus::Table, ServerStatus::Nickname),
            (ServerStatus::Table, ServerStatus::Fingerprint),
        ])
        .columns([
            (ExitNode::Table, ExitNode::ExitAddresses),
            (ExitNode::Table, ExitNode::Published),
        ])
        .left_join(
            ExitNode::Table,
            Expr::col((ExitNode::Table, ExitNode::Fingerprint))
                .equals((ServerStatus::Table, ServerStatus::Fingerprint))
                .and(
                    Expr::col((ExitNode::Table, ExitNode::Published)).equals(
                        (ServerStatus::Table, ServerStatus::Published),
                    ),
                ),
        )
        .to_owned();

    let is_bridge = match filters.r#type.unwrap_or(ParametersType::Relay) {
        ParametersType::Relay => false,
        ParametersType::Bridge => true,
    };

    q.and_where(Expr::col(ServerStatus::IsBridge).eq(is_bridge));

    if let Some(ref r#as) = filters.r#as {
        q.and_where(
            Expr::col((ServerStatus::Table, ServerStatus::AutonomousSystem))
                .eq(r#as),
        );
    }

    if let Some(ref fs) = filters.first_seen_since {
        let timestamp_stmt = format!("to_timestamp('{}', 'YYYY-MM-DD')", fs);
        q.and_where(
            Expr::col((ServerStatus::Table, ServerStatus::FirstSeen))
                .gte(timestamp_stmt),
        );
    }

    if let Some(ref ls) = filters.last_seen_since {
        let timestamp_stmt = format!("to_timestamp('{}', 'YYYY-MM-DD')", ls);
        q.and_where(
            Expr::col((ServerStatus::Table, ServerStatus::LastSeen))
                .gte(timestamp_stmt),
        );
    }

    if let Some(running) = filters.running {
        q.and_where(
            Expr::col((ServerStatus::Table, ServerStatus::Running))
                .eq(running),
        );
    }

    if let Some(ref country) = filters.country {
        q.and_where(
            Expr::col((ServerStatus::Table, ServerStatus::Country))
                .eq(country.as_ref()),
        );
    }

    if let Some(ref as_name) = filters.as_name {
        q.and_where(
            Expr::col((ServerStatus::Table, ServerStatus::AsName)).eq(as_name),
        );
    }

    if let Some(limit) = filters.limit {
        let limit = limit.as_ref().to_owned() as u64;
        q.limit(limit);
    }

    let (sql, values) = q.with(with).build_sqlx(PostgresQueryBuilder);
    sqlx::query_as_with::<_, ExitNodeRow, _>(&sql, values)
        .fetch_all(pg)
        .await
        .map_err(|e| e.to_string())
}
