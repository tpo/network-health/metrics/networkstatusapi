use std::sync::{Arc, RwLock};

use actix_web::{error::ErrorInternalServerError, web, Error, HttpResponse};
use apistos::api_operation;
use sqlx::PgPool;

use crate::{
    metrics,
    models::{
        factory::ResponseFactory,
        query::{domain::ParametersType, params::QueryFilters},
        responses::weights_probability::WeightsProbability,
    },
};

#[allow(clippy::await_holding_lock)]
#[api_operation(summary = "get weights probability")]
pub async fn get_weights_probability(
    params: QueryFilters,
    factory: web::Data<Arc<RwLock<ResponseFactory>>>,
    pg: web::Data<PgPool>,
) -> Result<HttpResponse, Error> {
    let lock = factory.read().unwrap();
    let mut response = lock.get();
    drop(lock);

    let res = tokio::try_join!(
        metrics::published(&pg, &params),
        metrics::weights_probability(&pg, &params)
    )
    .map_err(ErrorInternalServerError)?;

    let ((bp, rp), weights_probability) = res;

    response.relays_published(rp);
    response.bridges_published(bp);

    let wp = weights_probability
        .into_iter()
        .map(WeightsProbability::from)
        .collect();

    match params.r#type {
        Some(r#type) => match r#type {
            ParametersType::Relay => {
                response.relays(wp);
                response.bridges(vec![]);
            }
            ParametersType::Bridge => {
                response.bridges(wp);
                response.relays(vec![]);
            }
        },
        None => {
            response.bridges(wp.clone());
            response.relays(wp);
        }
    }

    let summary = response.build().map_err(ErrorInternalServerError)?;

    Ok(HttpResponse::Ok().json(summary))
}
