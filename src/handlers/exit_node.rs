use std::sync::{Arc, RwLock};

use actix_web::{error::ErrorInternalServerError, web, Error, HttpResponse};
use apistos::api_operation;
use sqlx::PgPool;

use crate::{
    metrics,
    models::{
        factory::ResponseFactory, query::params::QueryFilters,
        responses::exit_node::ExitNode,
    },
};

#[allow(clippy::await_holding_lock)]
#[api_operation(summary = "get exit")]
pub async fn get_exit_node(
    params: QueryFilters,
    factory: web::Data<Arc<RwLock<ResponseFactory>>>,
    pg: web::Data<PgPool>,
) -> Result<HttpResponse, Error> {
    let lock = factory.read().unwrap();
    let mut response = lock.get::<ExitNode, ExitNode>();
    drop(lock);

    let res = tokio::try_join!(
        metrics::published(&pg, &params),
        metrics::exit_addresses(&pg, &params),
    )
    .map_err(ErrorInternalServerError)?;

    let ((bp, rp), exit_addresses) = res;

    response.relays_published(rp);
    response.bridges_published(bp);
    response.bridges(vec![]);

    let exit_addresses =
        exit_addresses.into_iter().map(ExitNode::from).collect();
    response.relays(exit_addresses);

    let exit_node = response.build().map_err(ErrorInternalServerError)?;

    Ok(HttpResponse::Ok().json(exit_node))
}
