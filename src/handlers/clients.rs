use std::sync::Arc;

use actix_web::{web, Error, HttpResponse};
use apistos::api_operation;
use awc::http;

use crate::{
    models::query::{domain::ParametersType, params::VmProxyQueryFilters},
    victoriametrics::proxy::{ProxyRequestBuilder, VictoriaMetricsProxy},
};

#[api_operation(summary = "get clients")]
pub async fn get_clients(
    vm: web::Data<Arc<VictoriaMetricsProxy>>,
    params: VmProxyQueryFilters,
) -> Result<HttpResponse, Error> {
    // require fingerprint filter for the moment
    if params.lookup.is_none() {
        return Ok(HttpResponse::BadRequest().body("missing lookup parameter"));
    }

    let query = match params.r#type.unwrap_or(ParametersType::Relay) {
        ParametersType::Relay => {
            format!(
                r#"sum(max(desc_relay_dirreq_v3_responses{{fingerprint='{}'}})
                by (day) / 10)"#,
                params.lookup.unwrap().as_ref()
            )
        }
        ParametersType::Bridge => {
            format!(
                r#"sum(sum_over_time(max(desc_bridge_dirreq_v3_responses
                {{fingerprint='{}'}}))[24h]) by (day) / 10"#,
                params.lookup.unwrap().as_ref()
            )
        }
    };

    let req = ProxyRequestBuilder::new()
        .query(query)
        .start(params.start.unwrap_or("-10d".into()))
        .end(params.end.unwrap_or("-1d".into()));

    let res = vm.send(req).await;
    if res.status() != http::StatusCode::OK {
        return Ok(HttpResponse::InternalServerError().into());
    }

    Ok(res)
}
