use std::sync::{Arc, RwLock};

use actix_web::{error::ErrorInternalServerError, web, Error, HttpResponse};
use apistos::api_operation;
use sqlx::PgPool;

use crate::{
    metrics,
    models::{
        factory::ResponseFactory,
        query::{domain::ParametersType, params::QueryFilters},
        responses::summary::{BridgeSummary, RelaySummary},
    },
};

#[allow(clippy::await_holding_lock)]
#[api_operation(summary = "get relays/bridges summary")]
pub async fn get_summary(
    params: QueryFilters,
    factory: web::Data<Arc<RwLock<ResponseFactory>>>,
    pg: web::Data<PgPool>,
) -> Result<HttpResponse, Error> {
    let lock = factory.read().unwrap();
    let mut response = lock.get();
    drop(lock);

    let res = tokio::try_join!(
        metrics::published(&pg, &params),
        metrics::relay_summary(&pg, &params),
        metrics::bridges_summary(&pg, &params)
    )
    .map_err(ErrorInternalServerError)?;

    let ((bp, rp), relays, bridges) = res;

    response.relays_published(rp);
    response.bridges_published(bp);

    match params.r#type {
        Some(t) => match t {
            ParametersType::Relay => {
                let relays =
                    relays.into_iter().map(RelaySummary::from).collect();
                response.relays(relays);
                response.bridges(vec![]);
            }
            ParametersType::Bridge => {
                let bridges =
                    bridges.into_iter().map(BridgeSummary::from).collect();
                response.relays(vec![]);
                response.bridges(bridges);
            }
        },
        None => {
            let relays = relays.into_iter().map(RelaySummary::from).collect();
            let bridges =
                bridges.into_iter().map(BridgeSummary::from).collect();
            response.relays(relays);
            response.bridges(bridges);
        }
    }

    let summary = response.build().map_err(ErrorInternalServerError)?;

    Ok(HttpResponse::Ok().json(summary))
}
