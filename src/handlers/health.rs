use actix_web::{HttpResponse, Responder};
use apistos::api_operation;

#[api_operation(summary = "health check")]
pub async fn health() -> impl Responder {
    HttpResponse::Ok().body("OK")
}
