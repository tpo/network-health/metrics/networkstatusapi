use std::sync::Arc;

use actix_web::{web, Error, HttpResponse};
use apistos::api_operation;
use awc::http;

use crate::{
    models::query::{domain::ParametersType, params::VmProxyQueryFilters},
    victoriametrics::{
        labels::MetricsLabel,
        proxy::{ProxyRequestBuilder, VictoriaMetricsProxy},
    },
};

#[api_operation(summary = "get relays weights")]
pub async fn get_weights(
    vm: web::Data<Arc<VictoriaMetricsProxy>>,
    params: VmProxyQueryFilters,
) -> Result<HttpResponse, Error> {
    if params.r#type == Some(ParametersType::Bridge) {
        return Ok(HttpResponse::BadRequest()
            .body("metric not available for bridge type"));
    }

    let req = ProxyRequestBuilder::new()
        .label(
            MetricsLabel::NetworkExitFraction.into(),
            params.lookup.map(|x| x.into()),
        )
        .start(params.start.unwrap_or("-10d".into()))
        .end(params.end.unwrap_or("-1d".into()));

    let res = vm.send(req).await;
    if res.status() != http::StatusCode::OK {
        return Ok(HttpResponse::InternalServerError().into());
    }

    Ok(res)
}
