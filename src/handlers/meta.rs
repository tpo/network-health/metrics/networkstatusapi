use actix_web::web::Json;
use apistos::{api_operation, ApiComponent};
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

use crate::error::ErrorResponse;

#[derive(Serialize, Deserialize, Debug, Clone, JsonSchema, ApiComponent)]
pub struct Meta {
    pub nsapi_version: String,
    pub nsapi_build_git_hash: String,
}

#[api_operation(summary = "meta")]
pub async fn meta() -> Result<Json<Meta>, ErrorResponse> {
    let m = Meta {
        nsapi_version: env!("CARGO_PKG_VERSION").to_string(),
        nsapi_build_git_hash: env!("NSAPI_BUILD_GIT_HASH").to_owned(),
    };
    Ok(Json(m))
}
