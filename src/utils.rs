use crate::models::query::domain::PositiveOrZeroValue;

pub fn get_truncated(
    total: i32,
    limit: Option<PositiveOrZeroValue>,
    offset: Option<PositiveOrZeroValue>,
) -> i32 {
    let limit: i32 = match limit {
        Some(limit) => limit.into(),
        None => 0,
    };

    let offset = match offset {
        Some(offset) => offset.into(),
        None => 0,
    };

    total - limit - offset
}
