use std::net::TcpListener;

use sqlx::SqlitePool;

use crate::{
    models::factory::ResponseFactory, server::run,
    victoriametrics::proxy::VictoriaMetricsProxyConfig,
};

pub struct TestApp {
    pub addr: String,
}

#[allow(clippy::let_underscore_future)]
pub async fn spawn_app() -> TestApp {
    let listener = TcpListener::bind("127.0.0.1:0")
        .expect("failed to bind to random port");

    let port = listener.local_addr().unwrap().port();
    let addr = format!("http://127.0.0.1:{}", port);

    let conn_pool = configure_database().await;

    let factory = ResponseFactory::new();

    let vm_config = VictoriaMetricsProxyConfig::with(
        "testurl".into(),
        "testusername".into(),
        "testpasswdn".into(),
    );

    let server = run(listener, conn_pool, factory, vm_config)
        .expect("error running server");

    let _ = tokio::spawn(server);

    TestApp { addr }
}

async fn configure_database() -> SqlitePool {
    SqlitePool::connect(":memory:")
        .await
        .expect("could not connect to sqlite in-mem database")
}
