use std::{
    collections::HashSet,
    future::{ready, Ready},
};

use actix_web::{error::ErrorBadRequest, web, FromRequest};
use apistos::ApiComponent;
use derive_builder::Builder;
use lazy_static::lazy_static;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use sqlx::types::chrono::{DateTime, NaiveDateTime};

use super::domain::{
    CaseInsensitiveString, CountryCode, DaysAgoRange, LookupString,
    ParametersType, PositiveOrZeroValue, VersionType,
};

// Params for the API?
#[derive(
    Debug, Default, Builder, Serialize, Deserialize, JsonSchema, ApiComponent,
)]
#[builder(setter(strip_option), default)]
pub struct QueryParams {
    pub as_name: Option<String>,
    pub contact: Option<String>,
    pub country: Option<String>,
    pub family: Option<String>,
    pub fields: Option<String>,
    pub first_seen_days: Option<String>,
    pub first_seen_since: Option<String>,
    pub flag: Option<String>,
    pub host_name: Option<String>,
    pub last_seen_days: Option<String>,
    pub last_seen_since: Option<String>,
    pub limit: Option<i32>,
    pub lookup: Option<String>,
    pub offset: Option<i32>,
    pub order: Option<String>,
    pub os: Option<String>,
    pub published: Option<String>,
    pub r#as: Option<String>,
    pub r#type: Option<String>,
    pub recommended_version: Option<String>,
    pub running: Option<bool>,
    pub search: Option<String>,
    pub version: Option<String>,
}

// Filters for the API? Same fields as QueryParams but with String subtypes
#[derive(Debug, Default, Deserialize, JsonSchema, ApiComponent)]
pub struct QueryFilters {
    /// Return only relays with the parameter value matching (part of) the
    /// autonomous system (AS) name they are located in. If the parameter value
    /// contains spaces, only relays are returned which contain all
    /// space-separated parts in their AS name. Only printable ASCII characters
    /// are permitted in the parameter value, some of which need to be
    /// percent-encoded (# as %23, % as %25, & as %26, + as %2B, and / as %2F).
    /// Comparisons are case-insensitive.
    pub as_name: Option<String>,

    /// Return only relays with the parameter value matching (part of) the
    /// contact line. If the parameter value contains spaces, only relays are
    /// returned which contain all space-separated parts in their contact line.
    /// Only printable ASCII characters are permitted in the parameter value,
    /// some of which need to be percent-encoded (# as %23, % as %25, & as %26,
    /// + as %2B, and / as %2F). Comparisons are case-insensitive.
    pub contact: Option<CaseInsensitiveString>,

    /// Return only relays which are located in the given country as identified
    /// by a two-letter country code. Filtering by country code is
    /// case-insensitive. The special country code xz can be used for relays
    /// that were not found in the GeoIP database.
    // #[validate(length(min = 2, max = 2))]
    pub country: Option<CountryCode>,

    /// Return only the relay whose fingerprint matches the parameter value and
    /// all relays that this relay has listed in its family by fingerprint and
    /// that in turn have listed this relay in their family by fingerprint. If
    /// relays have listed other relays in their family by nickname, those
    /// family relationships will not be considered, regardless of whether they
    /// have the Named flag or not. The provided relay fingerprint must consist
    /// of 40 hex characters where case does not matter, and it must not be
    /// hashed using SHA-1. Bridges are not contained in the result, regardless
    /// of whether they define a family.
    pub family: Option<String>,

    /// Comma-separated list of fields that will be included in the result. So
    /// far, only top-level fields in relay or bridge objects of details
    /// documents can be specified, e.g., nickname,hashed_fingerprint. If the
    /// fields parameter is provided, all other fields which are not contained
    /// in the provided list will be removed from the result. Field names are
    /// case-insensitive.
    pub fields: Option<Vec<String>>,

    /// Return only relays or bridges which have first been seen during the
    /// given range of days ago. A parameter value "x-y" with x <= y returns
    /// relays or bridges that have first been seen at least x and at most y
    /// days ago. Accepted short forms are "x", "x-", and "-y" which are
    /// interpreted as "x-x", "x-infinity", and "0-y".
    pub first_seen_days: Option<DaysAgoRange>,

    /// Return only relays or bridges which have first been seen after the
    /// given date. The date has to be passed in the format "yyyy-MM-dd".
    pub first_seen_since: Option<String>,

    /// Return only relays which have the given relay flag assigned by the
    /// directory authorities. Note that if the flag parameter is specified
    /// more than once, only the first parameter value will be considered.
    /// Filtering by flag is case-insensitive.
    pub flag: Option<String>,

    /// Return only relays with a domain name ending in the given (partial)
    /// host name. Searches for subdomains of a specific domain should ideally
    /// be prefixed with a period, for example: ".csail.mit.edu". Non-ASCII
    /// host name characters must be encoded as punycode. Filtering by host
    /// name is case-insensitive.
    pub host_name: Option<CaseInsensitiveString>,

    /// Return only relays or bridges which have last been seen during the
    /// given range of days ago. A parameter value "x-y" with x <= y returns
    /// relays or bridges that have last been seen at least x and at most y
    /// days ago. Accepted short forms are "x", "x-", and "-y" which are
    /// interpreted as "x-x", "x-infinity", and "0-y". Note that relays and
    /// bridges that haven't been running in the past week are not included in
    /// results, so that setting x to 8 or higher will lead to an empty result
    /// set.
    pub last_seen_days: Option<DaysAgoRange>,

    /// Return only relays or bridges which have last been seen after the given
    /// date. The date has to be passed in the format "yyyy-MM-dd". Note that
    /// relays and bridges that haven't been running in the past week are not
    /// included in results, so that setting x to 8 or higher will lead to an
    /// empty result set.
    pub last_seen_since: Option<String>,

    /// Limit result to the given number of relays and/or bridges. Relays are
    /// kept first, then bridges. Non-positive limit values are treated as zero
    /// and lead to an empty result. When used together with offset, the
    /// offsetting step precedes the limiting step.
    pub limit: Option<PositiveOrZeroValue>,

    /// Return only the relay with the parameter value matching the fingerprint
    /// or the bridge with the parameter value matching the hashed fingerprint.
    /// Fingerprints should always be hashed using SHA-1, regardless of looking
    /// up a relay or a bridge, in order to not accidentally leak non-hashed
    /// bridge fingerprints in the URL. Lookups only work for full fingerprints
    /// or hashed fingerprints consisting of 40 hex characters. Lookups are
    /// case-insensitive.
    pub lookup: Option<LookupString>,

    /// Skip the given number of relays and/or bridges. Relays are skipped
    /// first, then bridges. Non-positive offset values are treated as zero and
    /// don't change the result.
    pub offset: Option<PositiveOrZeroValue>,

    /// Re-order results by a comma-separated list of fields in ascending or
    /// descending order. Results are first ordered by the first list element,
    /// then by the second, and so on. Possible fields for ordering are:
    /// consensus_weight and first_seen. Field names are case-insensitive.
    /// Ascending order is the default; descending order is selected by
    /// prepending fields with a minus sign (-). Field names can be listed at
    /// most once in either ascending or descending order. Relays or bridges
    /// which don't have any value for a field to be ordered by are always
    /// appended to the end, regardless or sorting order. The ordering is
    /// defined independent of the requested document type and does not require
    /// the ordering field to be contained in the document. If no order
    /// parameter is given, ordering of results is undefined.
    pub order: Option<CaseInsensitiveString>,

    /// Return only relays or bridges running on an operating system that
    /// starts with the parameter value. Searches are case-insensitive.
    pub os: Option<String>,

    /// Search results within one week of the passed published date. The date
    /// has to be passed in the format "yyyy-MM-dd".
    pub published: Option<String>,

    /// Return only relays which are located in either one of the given
    /// autonomous systems (AS) as identified by AS number (with or without
    /// preceding "AS" part). Multiple AS numbers can be provided separated by
    /// commas. Filtering by AS number is case-insensitive. The special AS
    /// number 0 can be used for relays that were not found in the GeoIP
    /// database.
    pub r#as: Option<String>,

    /// Return only relay (parameter value relay) or only bridge documents
    /// (parameter value bridge). Parameter values are case-insensitive.
    pub r#type: Option<ParametersType>,

    /// Return only relays and bridges running a Tor software version that is
    /// recommended (parameter value true) or not recommended by the directory
    /// authorities (parameter value false). Uses the version in the consensus
    /// or bridge network status. Relays and bridges are not contained in
    /// either result, if the version they are running is not known. Parameter
    /// values are case-insensitive.
    pub recommended_version: Option<CaseInsensitiveString>,

    /// Return only running (parameter value true) or only non-running relays
    /// and/or bridges (parameter value false). Parameter values are
    /// case-insensitive.
    pub running: Option<bool>,

    /// Return only (1) relays with the parameter value matching (part of a)
    /// nickname, (possibly $-prefixed) beginning of a hex-encoded fingerprint,
    /// any 4 hex character block of a space-separated fingerprint, beginning
    /// of a base64-encoded fingerprint without trailing equal signs, or
    /// beginning of an IP address (possibly enclosed in square brackets in
    /// case of IPv6), (2) bridges with (part of a) nickname or (possibly
    /// $-prefixed) beginning of a hashed hex-encoded fingerprint, and (3)
    /// relays and/or bridges matching a given qualified search term. Searches
    /// by relay IP address include all known addresses used for onion routing
    /// and for exiting to the Internet. Searches for beginnings of IP
    /// addresses are performed on textual representations of canonical IP
    /// address forms, so that searches using CIDR notation or non-canonical
    /// forms will return empty results. Searches are case-insensitive, except
    /// for base64-encoded fingerprints. If multiple search terms are given,
    /// separated by spaces, the intersection of all relays and bridges
    /// matching all search terms will be returned. Complete hex-encoded
    /// fingerprints should always be hashed using SHA-1, regardless of
    /// searching for a relay or a bridge, in order to not accidentally leak
    /// non-hashed bridge fingerprints in the URL. Qualified search terms have
    /// the form "key:value" (without double quotes) with "key" being one of
    /// the parameters listed here except for "search", "fingerprint", "order",
    /// "limit", "offset", and "fields", and "value" being the string that will
    /// internally be passed to that parameter. If a qualified search term for
    /// a given "key" is specified more than once, only the first "value" is
    /// considered.
    pub search: Option<String>,

    /// Return only relays or bridges running either Tor version from a list or
    /// range given in the parameter value. Tor versions must be provided
    /// without the leading "Tor" part. Multiple versions can either be
    /// provided as a comma-separated list (","), as a range separated by two
    /// dots (".."), or as a list of ranges. Provided versions are parsed and
    /// matched by parsed dotted numbers, rather than by string prefix.
    pub version: Option<VersionType>,
}

lazy_static! {
    static ref FIELDS: HashSet<&'static str> = {
        HashSet::from([
            "nickname",
            "fingerprint",
            "running",
            "flags",
            "country",
            "as",
            "as_name",
            "latitude",
            "longitude",
        ])
    };
}

impl TryFrom<QueryParams> for QueryFilters {
    type Error = String;

    fn try_from(value: QueryParams) -> Result<Self, Self::Error> {
        let mut s = Self::default();

        if let Some(r#type) = value.r#type {
            s.r#type = Some(ParametersType::try_from(r#type)?)
        }

        s.running = value.running;

        s.search = value.search;

        if let Some(lookup) = value.lookup {
            s.lookup = Some(LookupString::try_from(lookup)?);
        }

        if let Some(country) = value.country {
            s.country = Some(CountryCode::try_from(country)?)
        }

        s.r#as = value.r#as;

        if let Some(first_seen_days) = value.first_seen_days {
            s.first_seen_days = Some(DaysAgoRange::try_from(first_seen_days)?)
        }

        if let Some(last_seen_days) = value.last_seen_days {
            s.last_seen_days = Some(DaysAgoRange::try_from(last_seen_days)?)
        }

        if let Some(first_seen_since) = value.first_seen_since {
            DateTime::parse_from_str(first_seen_since.as_str(), "yyyy-MM-dd")
                .map_err(|_| "invalid date format.")?;

            s.first_seen_since = Some(first_seen_since);
        }

        if let Some(last_seen_since) = value.last_seen_since {
            DateTime::parse_from_str(last_seen_since.as_str(), "yyyy-MM-dd")
                .map_err(|_| "invalid date format.")?;

            s.last_seen_since = Some(last_seen_since);
        }

        if let Some(fields) = value.fields {
            let fields: Vec<_> = fields.split(',').collect();
            for f in &fields {
                if !FIELDS.contains(f) {
                    return Err(format!(r#""{}" is not a valid field."#, f));
                }
            }
            s.fields = Some(fields.into_iter().map(String::from).collect());
        }

        if let Some(contact) = value.contact {
            s.contact = Some(CaseInsensitiveString::try_from(contact)?)
        }

        s.family = value.family;

        if let Some(version) = value.version {
            s.version = Some(VersionType::try_from(version)?)
        }

        s.os = value.os;

        if let Some(host_name) = value.host_name {
            s.host_name = Some(CaseInsensitiveString::try_from(host_name)?)
        }

        if let Some(recommended_version) = value.recommended_version {
            s.recommended_version =
                Some(CaseInsensitiveString::try_from(recommended_version)?)
        }

        if let Some(order) = value.order {
            s.order = Some(CaseInsensitiveString::try_from(order)?)
        }

        if let Some(offset) = value.offset {
            s.offset = Some(PositiveOrZeroValue::from(offset));
        }

        if let Some(limit) = value.limit {
            s.limit = Some(PositiveOrZeroValue::from(limit));
        }

        if let Some(published) = value.published {
            // Append default time
            let datetime_str = format!("{}T00:00:00Z", published);
            NaiveDateTime::parse_from_str(
                datetime_str.as_str(),
                "%Y-%m-%dT%H:%M:%SZ",
            )
            .map_err(|_| "invalid date format.")?;

            // Set the parsed DateTime to s.published
            s.published = Some(datetime_str);
        }

        Ok(s)
    }
}
#[derive(Debug, Default, Builder, Serialize, Deserialize)]
#[builder(setter(strip_option), default)]
pub struct VmProxyQueryParams {
    pub r#type: Option<String>,
    pub lookup: Option<String>,
    pub start: Option<String>,
    pub end: Option<String>,
}

#[derive(Debug, Default, Deserialize, JsonSchema, ApiComponent)]
pub struct VmProxyQueryFilters {
    /// Return only relay (parameter value relay) or only bridge documents
    /// (parameter value bridge). Parameter values are case-insensitive.
    pub r#type: Option<ParametersType>,

    /// Return only the relay with the parameter value matching the fingerprint
    /// or the bridge with the parameter value matching the hashed fingerprint.
    /// Fingerprints should always be hashed using SHA-1, regardless of looking
    /// up a relay or a bridge, in order to not accidentally leak non-hashed
    /// bridge fingerprints in the URL. Lookups only work for full fingerprints
    /// or hashed fingerprints consisting of 40 hex characters. Lookups are
    /// case-insensitive.
    pub lookup: Option<LookupString>,

    /// The starting timestamp of the time range for query evaluation
    pub start: Option<String>,

    /// The ending timestamp of the time range for query evaluation. If the end
    /// isn't set, then the end is automatically set to the current time.
    pub end: Option<String>,
}

impl TryFrom<VmProxyQueryParams> for VmProxyQueryFilters {
    type Error = String;

    fn try_from(value: VmProxyQueryParams) -> Result<Self, Self::Error> {
        let mut s = Self::default();

        if let Some(r#type) = value.r#type {
            s.r#type = Some(ParametersType::try_from(r#type)?)
        }

        if let Some(lookup) = value.lookup {
            s.lookup = Some(LookupString::try_from(lookup)?);
        }

        s.start = value.start;
        s.end = value.end;

        Ok(s)
    }
}

impl FromRequest for QueryFilters {
    type Error = actix_web::Error;
    type Future = Ready<Result<Self, Self::Error>>;

    fn from_request(
        req: &actix_web::HttpRequest,
        _: &mut actix_web::dev::Payload,
    ) -> Self::Future {
        if let Ok(query_params) =
            web::Query::<QueryParams>::extract(req).into_inner()
        {
            return match QueryFilters::try_from(query_params.into_inner()) {
                Ok(filters) => ready(Ok(filters)),
                Err(e) => ready(Err(ErrorBadRequest(e))),
            };
        }

        ready(Err(ErrorBadRequest("incorrect query params.")))
    }
}

impl FromRequest for VmProxyQueryFilters {
    type Error = actix_web::Error;
    type Future = Ready<Result<Self, Self::Error>>;

    fn from_request(
        req: &actix_web::HttpRequest,
        _: &mut actix_web::dev::Payload,
    ) -> Self::Future {
        if let Ok(vm_query_params) =
            web::Query::<VmProxyQueryParams>::extract(req).into_inner()
        {
            return match VmProxyQueryFilters::try_from(
                vm_query_params.into_inner(),
            ) {
                Ok(filters) => ready(Ok(filters)),
                Err(e) => ready(Err(ErrorBadRequest(e))),
            };
        }

        ready(Err(ErrorBadRequest("incorrect query params.")))
    }
}
