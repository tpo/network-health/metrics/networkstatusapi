use std::fmt;

use apistos::{ApiType, InstanceType, TypedSchema};
use lazy_static::lazy_static;
use regex::Regex;
use serde::{Deserialize, Serialize};

/// String wrapper that always returns a lowercase, non-empty String
#[derive(Debug, Deserialize, ApiType)]
pub struct CaseInsensitiveString(String);

impl TryFrom<String> for CaseInsensitiveString {
    type Error = String;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        if value.is_empty() {
            return Err("case insensitive string cannot be empty".to_string());
        }

        Ok(Self(value.to_lowercase()))
    }
}

impl AsRef<str> for CaseInsensitiveString {
    fn as_ref(&self) -> &str {
        &self.0
    }
}

impl TypedSchema for CaseInsensitiveString {
    fn schema_type() -> InstanceType {
        InstanceType::String
    }

    fn format() -> Option<String> {
        None
    }
}
/// Wrapper for full fingerprints or hashed fingerprints
/// consisting of 40 hex characters.
/// Lookups are case-insensitive.
#[derive(Debug, Deserialize, ApiType)]
pub struct LookupString(CaseInsensitiveString);

impl TryFrom<String> for LookupString {
    type Error = String;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        if value.len() != 40 {
            return Err(
                r#"lookup param must be a 40 char long string containing hex
                chars"#
                    .to_string(),
            );
        }

        Ok(Self(CaseInsensitiveString(value)))
    }
}

impl AsRef<str> for LookupString {
    fn as_ref(&self) -> &str {
        self.0.as_ref()
    }
}

impl From<LookupString> for String {
    fn from(val: LookupString) -> Self {
        val.0 .0
    }
}

impl TypedSchema for LookupString {
    fn schema_type() -> InstanceType {
        InstanceType::String
    }

    fn format() -> Option<String> {
        None
    }
}
/// Wrapper for Country code string of length 2, case-insensitive
#[derive(Debug, Deserialize, ApiType)]
pub struct CountryCode(CaseInsensitiveString);

impl TryFrom<String> for CountryCode {
    type Error = String;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        if value.len() != 2 {
            return Err("country code must be two chars long.".to_string());
        }

        Ok(Self(CaseInsensitiveString(value)))
    }
}

impl AsRef<str> for CountryCode {
    fn as_ref(&self) -> &str {
        self.0.as_ref()
    }
}

impl TypedSchema for CountryCode {
    fn schema_type() -> InstanceType {
        InstanceType::String
    }

    fn format() -> Option<String> {
        None
    }
}
/// Filter on types
///
/// https://metrics.torproject.org/onionoo.html#parameters_type
///
/// Can only be **relay** or **bridge**
#[derive(
    Debug, Serialize, Deserialize, Clone, Copy, PartialEq, Eq, ApiType,
)]
pub enum ParametersType {
    Relay,
    Bridge,
}

impl TryFrom<String> for ParametersType {
    type Error = String;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        match value.to_lowercase().as_str() {
            "relay" => Ok(ParametersType::Relay),
            "bridge" => Ok(ParametersType::Bridge),
            _ => Err("invalid data type".to_string()),
        }
    }
}

impl fmt::Display for ParametersType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            ParametersType::Relay => write!(f, "relay"),
            ParametersType::Bridge => write!(f, "bridge"),
        }
    }
}

impl TypedSchema for ParametersType {
    fn schema_type() -> InstanceType {
        InstanceType::String
    }

    fn format() -> Option<String> {
        None
    }
}
/// Wrapper that transforms negative values to zero and
/// keeps the value of positive values.
#[derive(
    Debug, Serialize, Deserialize, Clone, Copy, PartialEq, Eq, ApiType,
)]
pub struct PositiveOrZeroValue(i32);

impl From<i32> for PositiveOrZeroValue {
    fn from(value: i32) -> Self {
        if value.is_negative() {
            return Self(0);
        }

        Self(value)
    }
}

impl AsRef<i32> for PositiveOrZeroValue {
    fn as_ref(&self) -> &i32 {
        &self.0
    }
}

impl From<PositiveOrZeroValue> for i32 {
    fn from(val: PositiveOrZeroValue) -> Self {
        val.0
    }
}

impl TypedSchema for PositiveOrZeroValue {
    fn schema_type() -> InstanceType {
        InstanceType::String
    }

    fn format() -> Option<String> {
        None
    }
}
/// Wrapper for valid Tor Version
/// Specs can be found at
/// https://gitlab.torproject.org/tpo/core/torspec/-/blob/main/version-spec.txt
#[derive(Debug, Serialize, Deserialize, PartialEq, Eq, ApiType)]
pub struct Version {
    pub major: u8,
    pub minor: u8,
    pub micro: u8,
    pub patchlevel: u8,
    pub cvs: Option<String>,
}

impl TryFrom<String> for Version {
    type Error = String;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        lazy_static! {
            static ref RE: Regex = Regex::new(
                r"^(?P<MAJOR>\d+)\.(?P<MINOR>\d+)\.(?P<MICRO>\d+)\.(?P<PATCHLEVEL>\d+)(?P<CVS>-[A-Za-z]+)*$" // editorconfig-checker-disable-line
            )
            .unwrap();
        }

        let caps = match RE.captures(&value) {
            None => return Err("invalid version.".to_string()),
            Some(caps) => caps,
        };

        Ok(Self {
            major: caps["MAJOR"]
                .parse()
                .map_err(|_| "major version is nan.")?,
            minor: caps["MINOR"]
                .parse()
                .map_err(|_| "minor version is nan.")?,
            micro: caps["MICRO"]
                .parse()
                .map_err(|_| "micro version is nan.")?,
            patchlevel: caps["PATCHLEVEL"]
                .parse()
                .map_err(|_| "patchlevel version is nan.")?,
            cvs: caps.name("CVS").map(|v| v.as_str().into()),
        })
    }
}
impl TypedSchema for Version {
    fn schema_type() -> InstanceType {
        InstanceType::String
    }

    fn format() -> Option<String> {
        None
    }
}

/// Wrapper for valid tor versions.
/// Valid types are "**,**" separated values,
/// "**..**" ranges and single values
#[derive(Debug, Serialize, Deserialize, PartialEq, Eq, ApiType)]
pub enum VersionType {
    Range(Version, Version),
    List(Vec<Version>),
    Value(Version),
}

impl TryFrom<String> for VersionType {
    type Error = String;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        if value.contains("..") {
            // Must be a valid range of version
            if let Some((s1, s2)) = value.split_once("..") {
                if let (Ok(v1), Ok(v2)) = (
                    Version::try_from(s1.to_string()),
                    Version::try_from(s2.to_string()),
                ) {
                    return Ok(Self::Range(v1, v2));
                }
            }
        } else if value.contains(',') {
            // Must be a valid list of versions
            let versions: Vec<Result<Version, String>> = value
                .split(',')
                .map(|s| Version::try_from(s.to_string()))
                .collect();

            for v in &versions {
                if v.is_err() {
                    return Err("list contains invalid version.".to_string());
                }
            }

            return Ok(Self::List(
                versions.into_iter().map(|v| v.unwrap()).collect(),
            ));
        }

        if let Ok(v) = Version::try_from(value) {
            return Ok(Self::Value(v));
        }

        Err("invalid version.".to_string())
    }
}

impl TypedSchema for VersionType {
    fn schema_type() -> InstanceType {
        InstanceType::String
    }

    fn format() -> Option<String> {
        None
    }
}

/// Wrapper for valid range of days
#[derive(Debug, Serialize, Deserialize, PartialEq, Eq, ApiType)]
pub enum DaysAgoRange {
    AtMost(u8),
    AtLeast(u8),
    Range(u8, u8),
}

impl TryFrom<String> for DaysAgoRange {
    type Error = String;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        match value.split_once('-') {
            Some(("", max)) => {
                let max = max
                    .parse()
                    .map_err(|_| "invalid day range".to_string())?;

                Ok(Self::AtMost(max))
            }
            Some((min, "")) => {
                let min = min
                    .parse()
                    .map_err(|_| "invalid day range".to_string())?;

                Ok(Self::AtLeast(min))
            }
            Some((min, max)) => {
                let min = min
                    .parse()
                    .map_err(|_| "invalid day range".to_string())?;
                let max = max
                    .parse()
                    .map_err(|_| "invalid day range".to_string())?;

                Ok(Self::Range(min, max))
            }
            _ => {
                let v = value
                    .parse()
                    .map_err(|_| "invalid day range.".to_string())?;

                Ok(Self::Range(v, v))
            }
        }
    }
}

impl TypedSchema for DaysAgoRange {
    fn schema_type() -> InstanceType {
        InstanceType::String
    }

    fn format() -> Option<String> {
        None
    }
}
