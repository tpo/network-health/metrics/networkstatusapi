use apistos::ApiComponent;
use derive_builder::Builder;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use serde_with::skip_serializing_none;

use super::AsStringVec;
use crate::metrics;

#[skip_serializing_none]
#[derive(
    Debug, Clone, Serialize, Deserialize, Builder, JsonSchema, ApiComponent,
)]
pub struct ExitNode {
    pub nickname: String,

    /// Relay fingerprint consisting of 40 upper-case hexadecimal characters.
    pub fingerprint: String,

    pub published: String,

    /// Array of IPv4 addresses that the relay used to exit to the Internet in
    /// the past 24 hours. Omitted if array is empty.
    pub exit_addresses: Vec<String>,
}

impl From<metrics::ExitNodeRow> for ExitNode {
    fn from(value: metrics::ExitNodeRow) -> Self {
        let mut en = ExitNodeBuilder::default();
        en.nickname(value.nickname);
        en.fingerprint(value.fingerprint);
        en.published(value.published.to_string());
        en.exit_addresses(value.exit_addresses.as_string_vec());
        en.build().unwrap()
    }
}

#[cfg(test)]
mod tests {
    use sqlx::types::chrono::NaiveDate;

    use super::*;
    use crate::metrics::ExitNodeRow;

    #[test]
    fn test_from_enrow() {
        let nickname = "nickname".into();
        let fingerprint = "1".repeat(40);
        let published = NaiveDate::from_ymd_opt(2020, 7, 8)
            .unwrap()
            .and_hms_opt(0, 0, 0)
            .unwrap();
        let exit_addresses = r#"["1.1.1.1"]"#.into();

        let exit_node_row = ExitNodeRow {
            nickname,
            fingerprint,
            published,
            exit_addresses,
        };

        let exit_node = ExitNode::from(exit_node_row.clone());

        assert_eq!(exit_node.fingerprint, exit_node_row.fingerprint);
        assert_eq!(exit_node.nickname, exit_node_row.nickname);
        assert_eq!(exit_node.published, "2020-07-08 00:00:00".to_string());
        assert_eq!(exit_node.exit_addresses, vec!["1.1.1.1"]);
    }
}
