use sqlx::types::chrono;

pub mod bandwidth;
pub mod details;
pub mod exit_node;
pub mod generic;
pub mod summary;
pub mod weights_probability;

trait AsStringVec {
    fn as_string_vec(&self) -> Vec<String>;
}

impl AsStringVec for String {
    fn as_string_vec(&self) -> Vec<String> {
        serde_json::from_str(self).unwrap_or_default()
    }
}

impl AsStringVec for Option<String> {
    fn as_string_vec(&self) -> Vec<String> {
        match &self {
            Some(v) => v.as_string_vec(),
            None => vec![],
        }
    }
}

trait TimestampProcessor {
    fn process_timestamp(&self) -> String;
}

// Implement the trait for Option<NaiveDateTime>
impl TimestampProcessor for Option<chrono::NaiveDateTime> {
    fn process_timestamp(&self) -> String {
        match self {
            Some(datetime) => datetime.format("%Y-%m-%d %H:%M:%S").to_string(),
            None => String::from(""), // Handle the None case
        }
    }
}
