use derive_builder::Builder;
use serde::{Deserialize, Serialize};
use serde_with::skip_serializing_none;

#[skip_serializing_none]
#[derive(Debug, Clone, Serialize, Deserialize, Default, Builder)]
pub struct Response<R, B> {
    /// Network Status API protocol version string.
    pub version: &'static str,

    /// UTC date (YYYY-MM-DD) when the next major protocol version is scheduled
    /// to be deployed. Omitted if no major protocol changes are planned.
    pub next_major_version_scheduled: Option<String>,

    /// Git revision of the Onionoo instance's software used to write this
    /// response, which will be omitted if unknown.
    pub build_version: Option<&'static str>,

    /// UTC timestamp (YYYY-MM-DD hh:mm:ss) when the last known relay network
    /// status consensus started being valid. Indicates how recent the relay
    /// objects in this document are.
    pub relays_published: String,

    /// Array of relay objects as specified below.
    pub relays: Vec<R>,

    /// UTC timestamp (YYYY-MM-DD hh:mm:ss) when the last known bridge network
    /// status was published. Indicates how recent the bridge objects in this
    /// document are.
    pub bridges_published: String,

    /// Array of bridge objects as specified below.
    pub bridges: Vec<B>,
}
