use apistos::ApiComponent;
use derive_builder::Builder;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

use crate::metrics;

#[derive(
    Debug, Clone, Serialize, Deserialize, Builder, JsonSchema, ApiComponent,
)]
pub struct WeightsProbability {
    pub nickname: String,
    pub fingerprint: String,
    pub guard_weight_fraction: f64,
    pub middle_weight_fraction: f64,
    pub exit_weight_fraction: f64,
    pub time: String,
    pub weights_id: String,
}

impl From<metrics::WeightsProbabilityRow> for WeightsProbability {
    fn from(value: metrics::WeightsProbabilityRow) -> Self {
        let mut weights_probability = WeightsProbabilityBuilder::default();
        weights_probability.nickname(value.nickname);
        weights_probability.fingerprint(value.fingerprint);
        weights_probability.time(value.time.to_string());
        weights_probability.weights_id(value.weights_id.to_string());
        weights_probability.exit_weight_fraction(value.exit_weight_fraction);
        weights_probability.guard_weight_fraction(value.guard_weight_fraction);
        weights_probability
            .middle_weight_fraction(value.middle_weight_fraction);
        weights_probability.build().unwrap()
    }
}
