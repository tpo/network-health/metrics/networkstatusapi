use apistos::ApiComponent;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

use crate::metrics::{BridgeSummaryRow, RelaySummaryRow};

#[derive(Debug, Serialize, Deserialize, Clone, JsonSchema, ApiComponent)]
pub struct RelaySummary {
    /// Relay nickname consisting of 1-19 alphanumerical characters.
    pub n: String,

    /// Relay fingerprint consisting of 40 upper-case hexadecimal characters.
    pub f: String,

    /// Array of IPv4 or IPv6 addresses where the relay accepts onion-routing
    /// connections or which the relay used to exit to the Internet in the past
    /// 24 hours. The first address is the primary onion-routing address that
    /// the relay used to register in the network, subsequent addresses are in
    /// arbitrary order. IPv6 hex characters are all lower-case.
    pub a: Vec<String>,

    /// Boolean field saying whether this relay was listed as running in the
    /// last relay network status consensus.
    pub r: bool,
}

impl From<RelaySummaryRow> for RelaySummary {
    fn from(value: RelaySummaryRow) -> Self {
        let or_addresses: Vec<_> =
            serde_json::from_str(&value.or_addresses).unwrap_or_default();

        Self {
            n: value.nickname,
            f: value.fingerprint,
            a: or_addresses,
            r: value.running.unwrap_or(false),
        }
    }
}

#[derive(
    Debug,
    Serialize,
    Deserialize,
    Clone,
    sqlx::FromRow,
    JsonSchema,
    ApiComponent,
)]
pub struct BridgeSummary {
    /// Relay nickname consisting of 1–19 alphanumerical characters.
    pub n: String,

    /// SHA-1 hash of the bridge fingerprint consisting of 40 upper-case
    /// hexadecimal characters.
    pub h: String,

    /// Boolean field saying whether this bridge was listed as running in the
    /// last bridge network status.
    pub r: bool,
}

impl From<BridgeSummaryRow> for BridgeSummary {
    fn from(value: BridgeSummaryRow) -> Self {
        Self {
            n: value.nickname,
            h: value.fingerprint,
            r: value.running.unwrap_or(false),
        }
    }
}
