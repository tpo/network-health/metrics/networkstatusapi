use apistos::ApiComponent;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use serde_with::skip_serializing_none;

use crate::{
    metrics::{BridgeBandwidthRow, RelayBandwidthRow},
    victoriametrics::{
        labels::MetricsLabel, VictoriaMetricsRef, VictoriaMetricsRefBuilder,
    },
};

#[skip_serializing_none]
#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema, ApiComponent)]
pub struct RelayBandwidth {
    /// Relay fingerprint consisting of 40 upper-case hexadecimal characters.
    pub fingerprint: String,

    /// Object containing graph history objects with written bytes for
    /// different time periods. Keys are string representation of the time
    /// period covered by the graph history object. Keys are fixed strings
    /// "1_month", "6_months", "1_year", and "5_years". Keys refer to the last
    /// known bandwidth history of a relay, not to the time when the bandwidth
    /// document was published. Similarly, a graph history object is excluded
    /// if the relay did not provide bandwidth histories on the required level
    /// of detail. The unit is bytes per second. Contained graph history
    /// objects may contain null values if the relay did not provide any
    /// bandwidth data or only data for less than 20% of a given time period.
    pub write_history: Option<VictoriaMetricsRef>,

    /// Object containing graph history objects with read bytes for different
    /// time periods. The specification of graph history objects is similar to
    /// those in the write_history field.
    pub read_history: Option<VictoriaMetricsRef>,

    /// Json object containing the overload-ratelimits information for the
    /// relay. Missing if no bandwidth limit was reached for this relay. Keys
    /// are fixed strings timestamp, rate-limit, burst-limit,
    /// read-overload-count, write-overload-count. The "rate-limit" and
    /// "burst-limit" are the raw values from the BandwidthRate and
    /// BandwidthBurst found in the torrc configuration file. The
    /// "{read|write}-overload-count" are the counts of how many times the
    /// reported limits of burst/rate were exhausted and thus the maximum
    /// between the read and write count occurrences
    pub overload_ratelimits: Option<i64>,

    /// Json object containing the overload-fd-exhausted information for the
    /// relay. Missing if no file descriptor was exhausted for this relay.
    /// Indicates that a file descriptor exhaustion was experienced by this
    /// relay. Keys are fixed strings timestamp.
    pub overload_fd_exhausted: Option<String>,
}

impl RelayBandwidth {
    pub fn new(
        fingerprint: String,
        write_history: Option<VictoriaMetricsRef>,
        read_history: Option<VictoriaMetricsRef>,
        overload_ratelimits: Option<i64>,
        overload_fd_exhausted: Option<String>,
    ) -> Self {
        Self {
            fingerprint,
            write_history,
            read_history,
            overload_ratelimits,
            overload_fd_exhausted,
        }
    }
}

impl From<RelayBandwidthRow> for RelayBandwidth {
    fn from(rbr: RelayBandwidthRow) -> Self {
        let write_history = VictoriaMetricsRefBuilder::default()
            .with_metric(MetricsLabel::WriteBandwidthHistory)
            .with_fingerprint(rbr.fingerprint.clone())
            .build()
            .unwrap();

        let read_history = VictoriaMetricsRefBuilder::default()
            .with_metric(MetricsLabel::ReadBandwidthHistory)
            .with_fingerprint(rbr.fingerprint.clone())
            .build()
            .unwrap();

        RelayBandwidth::new(
            rbr.fingerprint,
            Some(write_history),
            Some(read_history),
            rbr.overload_ratelimits_ratelimit,
            rbr.overload_fd_exhausted_timestamp.map(|x| x.to_string()),
        )
    }
}

#[skip_serializing_none]
#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema, ApiComponent)]
pub struct BridgeBandwidth {
    /// SHA-1 hash of the bridge fingerprint consisting of 40 upper-case
    /// hexadecimal characters.
    pub fingerprint: String,

    /// Object containing graph history objects with written bytes for
    /// different time periods. The specification of graph history objects is
    /// similar to those in the write_history field of relays.
    pub write_history: Option<VictoriaMetricsRef>,

    /// Object containing graph history objects with read bytes for different
    /// time periods. The specification of graph history objects is similar to
    /// those in the write_history field of relays.
    pub read_history: Option<VictoriaMetricsRef>,

    /// Json object containing the overload-ratelimits information for the
    /// relay. Missing if no bandwidth limit was reached for this relay. Keys
    /// are fixed strings timestamp, rate-limit, burst-limit,
    /// read-overload-count, write-overload-count. The "rate-limit" and
    /// "burst-limit" are the raw values from the BandwidthRate and
    /// BandwidthBurst found in the torrc configuration file. The
    /// "{read|write}-overload-count" are the counts of how many times the
    /// reported limits of burst/rate were exhausted and thus the maximum
    /// between the read and write count occurrences
    pub overload_ratelimits: Option<i64>,

    /// Json object containing the overload-fd-exhausted information for the
    /// relay. Missing if no file descriptor was exhausted for this relay.
    /// Indicates that a file descriptor exhaustion was experienced by this
    /// relay. Keys are fixed strings timestamp.
    pub overload_fd_exhausted: Option<String>,
}

impl BridgeBandwidth {
    pub fn new(
        fingerprint: String,
        write_history: Option<VictoriaMetricsRef>,
        read_history: Option<VictoriaMetricsRef>,
        overload_ratelimits: Option<i64>,
        overload_fd_exhausted: Option<String>,
    ) -> Self {
        Self {
            fingerprint,
            write_history,
            read_history,
            overload_ratelimits,
            overload_fd_exhausted,
        }
    }
}

impl From<BridgeBandwidthRow> for BridgeBandwidth {
    fn from(bbr: BridgeBandwidthRow) -> Self {
        let write_history = VictoriaMetricsRefBuilder::default()
            .with_metric(MetricsLabel::WriteBandwidthHistory)
            .with_fingerprint(bbr.fingerprint.clone())
            .build()
            .unwrap();

        let read_history = VictoriaMetricsRefBuilder::default()
            .with_metric(MetricsLabel::ReadBandwidthHistory)
            .with_fingerprint(bbr.fingerprint.clone())
            .build()
            .unwrap();

        BridgeBandwidth::new(
            bbr.fingerprint,
            Some(write_history),
            Some(read_history),
            bbr.overload_ratelimits_ratelimit,
            bbr.overload_fd_exhausted_timestamp.map(|x| x.to_string()),
        )
    }
}
