use apistos::ApiComponent;
use derive_builder::Builder;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use serde_with::skip_serializing_none;

use super::{AsStringVec, TimestampProcessor};
use crate::metrics;

#[skip_serializing_none]
#[derive(
    Debug, Clone, Serialize, Deserialize, Builder, JsonSchema, ApiComponent,
)]
pub struct RelayDetails {
    /// Relay nickname consisting of 1-19 alphanumerical characters.
    pub nickname: String,

    /// Relay fingerprint consisting of 40 upper-case hexadecimal characters.
    pub fingerprint: String,

    /// Array of IPv4 or IPv6 addresses and TCP ports or port lists where the
    /// relay accepts onion-routing connections. The first address is the
    /// primary onion-routing address that the relay used to register in the
    /// network, subsequent addresses are in arbitrary order. IPv6 hex
    /// characters are all lower-case.
    pub or_addresses: Vec<String>,

    /// IPv4 address and TCP port where the relay accepts directory
    /// connections. Omitted if the relay does not accept directory
    /// connections.
    pub dir_address: String,

    /// UTC timestamp (YYYY-MM-DD hh:mm:ss) when this relay was last seen in a
    /// network status consensus.
    pub last_seen: String,

    /// UTC timestamp (YYYY-MM-DD hh:mm:ss) when this relay was first seen in a
    /// network status consensus.
    pub first_seen: String,

    /// Boolean field saying whether this relay was listed as running in the
    /// last relay network status consensus.
    pub running: bool,

    /// Array of relay flags that the directory authorities assigned to this
    /// relay. May be omitted if empty.
    pub flags: Vec<String>,

    /// Two-letter lower-case country code as found in a GeoIP database by
    /// resolving the relay's first onion-routing IP address. Omitted if the
    /// relay IP address could not be found in the GeoIP database.
    pub country: Option<String>,

    /// Country name as found in a GeoIP database by resolving the relay's
    /// first onion-routing IP address. Omitted if the relay IP address could
    /// not be found in the GeoIP database, or if the GeoIP database did not
    /// contain a country name.
    pub country_name: Option<String>,

    /// AS number as found in an AS database by resolving the relay's first
    /// onion-routing IP address. AS number strings start with "AS", followed
    /// directly by the AS number. Omitted if the relay IP address could not be
    /// found in the AS database.
    pub r#as: Option<String>,

    /// AS name as found in an AS database by resolving the relay's first
    /// onion-routing IP address. Omitted if the relay IP address could not be
    /// found in the AS database.
    pub as_name: Option<String>,

    /// Host names as found in a reverse DNS lookup of the relay's primary IP
    /// address for which a matching A record was also found. This field is
    /// updated at most once in 12 hours, unless the relay IP address changes.
    /// Omitted if the relay IP address was not looked up, if no lookup request
    /// was successful yet, or if no A records were found matching the PTR
    /// records (i.e. it was not possible to verify the value of any of the PTR
    /// records). A DNSSEC validating resolver is used for these lookups.
    /// Failure to validate DNSSEC signatures will prevent those names from
    /// appearing in this field.
    pub verified_host_names: Vec<String>,

    /// UTC timestamp (YYYY-MM-DD hh:mm:ss) when the relay was last
    /// (re-)started. Missing if router descriptor containing this information
    /// cannot be found.
    pub last_restarted: String,

    /// Array of exit-policy lines. Missing if router descriptor containing
    /// this information cannot be found. May contradict the
    /// "exit_policy_summary" field in a rare edge case: this happens when the
    /// relay changes its exit policy after the directory authorities
    /// summarized the previous exit policy.
    pub exit_policy: Vec<String>,

    /// Contact address of the relay operator. Omitted if empty or if
    /// descriptor containing this information cannot be found.
    pub contact: Option<String>,

    /// Platform string containing operating system and Tor version details.
    /// Omitted if empty or if descriptor containing this information cannot be
    /// found.
    pub platform: Option<String>,

    /// Tor software version without leading "Tor" as reported by the directory
    /// authorities in the "v" line of the consensus. Omitted if either the
    /// directory authorities or the relay did not report which version the
    /// relay runs or if the relay runs an alternative Tor implementation.
    pub version: Option<String>,

    /// Boolean field saying whether the Tor software version of this relay is
    /// recommended by the directory authorities or not. Uses the relay version
    /// in the consensus. Omitted if either the directory authorities did not
    /// recommend versions, or the relay did not report which version it runs.
    pub recommended_version: Option<bool>,

    /// Status of the Tor software version of this relay based on the versions
    /// recommended by the directory authorities. Possible version statuses
    /// are: "recommended" if a version is listed as recommended;
    /// "experimental" if a version is newer than every recommended version;
    /// "obsolete" if a version is older than every recommended version; "new
    /// in series" if a version has other recommended versions with the same
    /// first three components, and the version is newer than all such
    /// recommended versions, but it is not newer than every recommended
    /// version; "unrecommended" if none of the above conditions hold. Omitted
    /// if either the directory authorities did not recommend versions, or the
    /// relay did not report which version it runs.
    pub version_status: Option<String>,

    /// Array of fingerprints of relays that are in an effective, mutual family
    /// relationship with this relay. These relays are part of this relay's
    /// family and they consider this relay to be part of their family. Always
    /// contains the relay's own fingerprint. Omitted if the descriptor
    /// containing this information cannot be found.
    pub effective_family: Vec<String>,

    /// Array of fingerprints of relays that are not in an effective, mutual
    /// family relationship with this relay. These relays are part of this
    /// relay's family but they don't consider this relay to be part of their
    /// family. Omitted if empty or if descriptor containing this information
    /// cannot be found.
    pub alleged_family: Option<Vec<String>>,

    /// Array of fingerprints of relays that are not in an effective, mutual
    /// family relationship with this relay but that can be reached by
    /// following effective, mutual family relationships starting at this
    /// relay. Omitted if empty or if descriptor containing this information
    /// cannot be found.
    pub indirect_family: Option<Vec<String>>,

    /// UTC timestamp (YYYY-MM-DD hh:mm:ss) when this relay last stopped
    /// announcing an IPv4 or IPv6 address or TCP port where it previously
    /// accepted onion-routing or directory connections. This timestamp can
    /// serve as indicator whether this relay would be a suitable fallback
    /// directory.
    pub last_changed_address_or_port: String,

    /// Boolean field saying whether this relay indicated that it is
    /// hibernating in its last known server descriptor. This information may
    /// be helpful to decide whether a relay that is not running anymore has
    /// reached its accounting limit and has not dropped out of the network for
    /// another, unknown reason. Omitted if either the relay is not
    /// hibernating, or if no information is available about the hibernation
    /// status of the relay.
    pub hibernating: Option<bool>,

    /// Host names as found in a reverse DNS lookup of the relay's primary IP
    /// address that for which a matching A record was not found. This field is
    /// updated at most once in 12 hours, unless the relay IP address changes.
    /// Omitted if the relay IP address was not looked up, if no lookup request
    /// was successful yet, or if A records were found matching all PTR records
    /// (i.e. it was possible to verify the value of each of the PTR records).
    /// A DNSSEC validating resolver is used for these lookups. Failure to
    /// validate DNSSEC signatures will prevent those names from appearing in
    /// this field.
    pub unverified_host_names: Vec<String>,

    /// Average bandwidth in bytes per second that this relay is willing to
    /// sustain over long periods. Missing if router descriptor containing this
    /// information cannot be found.
    pub bandwidth_avg: Option<i64>,

    /// Bandwidth in bytes per second that this relay is willing to sustain in
    /// very short intervals. Missing if router descriptor containing this
    /// information cannot be found.
    pub bandwidth_burst: Option<i64>,

    /// Bandwidth estimate in bytes per second of the capacity this relay can
    /// handle. The relay remembers the maximum bandwidth sustained output over
    /// any ten second period in the past day, and another sustained input. The
    /// "observed_bandwidth" value is the lesser of these two numbers. Missing
    /// if router descriptor containing this information cannot be found.
    pub observed_bandwidth: Option<i64>,

    /// Bandwidth in bytes per second that this relay is willing and capable to
    /// provide. This bandwidth value is the minimum of bandwidth_avg,
    /// bandwidth_burst, and observed_bandwidth. Missing if router descriptor
    /// containing this information cannot be found.
    pub advertised_bandwidth: Option<i64>,

    /// Indicates that a relay has reached an "overloaded state" which can be
    /// one or many of the following load metrics:
    /// - Any OOM invocation due to memory pressure
    /// - Any ntor onionskins are dropped
    /// - TCP port exhaustion
    ///
    /// The timestamp is when at least one metrics was detected.
    pub overload_general_timestamp: String,

    pub overload_fd_exhausted_timestamp: String,
    pub diff_or_addresses: String,

    /// Summary version of the relay's IPv6 exit policy containing a dictionary
    /// with either an "accept" or a "reject" element. If there is an "accept"
    /// ("reject") element, the relay accepts (rejects) all TCP ports or port
    /// ranges in the given list for most IP addresses and rejects (accepts)
    /// all other ports. Missing if the relay rejects all connections to IPv6
    /// addresses. May contradict the "exit_policy_summary" field in a rare
    /// edge case: this happens when the relay changes its exit policy after
    /// the directory authorities summarized the previous exit policy.
    pub exit_policy_v6_summary: Option<String>,

    /// Boolean field saying whether the consensus weight of this relay is
    /// based on a threshold of 3 or more measurements by Tor bandwidth
    /// authorities. Omitted if the network status consensus containing this
    /// relay does not contain measurement information.
    pub measured: Option<bool>,

    /// Array of IPv4 or IPv6 addresses and TCP ports or port lists where the
    /// relay claims in its descriptor to accept onion-routing connections but
    /// that the directory authorities failed to confirm as reachable. Contains
    /// only additional addresses of a relay that are found unreachable and
    /// only as long as a minority of directory authorities performs
    /// reachability tests on these additional addresses. Relays with an
    /// unreachable primary address are not included in the network status
    /// consensus and excluded entirely. Likewise, relays with unreachable
    /// additional addresses tested by a majority of directory authorities are
    /// not included in the network status consensus and excluded here, too. If
    /// at any point network status votes will be added to the processing,
    /// relays with unreachable addresses will be included here. Addresses are
    /// in arbitrary order. IPv6 hex characters are all lower-case. Omitted if
    /// empty.
    pub unreachable_or_addresses: Option<Vec<String>>,
}

impl From<metrics::RelayDetailsRow> for RelayDetails {
    fn from(value: metrics::RelayDetailsRow) -> Self {
        let mut relay_details = RelayDetailsBuilder::default();
        relay_details.nickname(value.nickname);
        relay_details.fingerprint(value.fingerprint);
        relay_details.or_addresses(value.or_addresses.as_string_vec());

        relay_details.dir_address(value.dir_port.unwrap_or(0).to_string());

        relay_details.last_seen(value.last_seen.to_string());
        // TODO fix as it's set empty for now
        relay_details.last_changed_address_or_port(
            value.last_changed_address_or_port.to_string(),
        );
        relay_details.first_seen(value.first_seen.to_string());
        relay_details.running(value.running.unwrap_or(false));
        relay_details.hibernating(value.is_hibernating);
        relay_details.flags(value.flags.as_string_vec());
        relay_details.country(value.country);
        relay_details.country_name(value.country_name);
        relay_details.r#as(value.autonomous_system);
        relay_details.as_name(value.as_name);
        // TODO
        relay_details.alleged_family(None);
        relay_details
            .verified_host_names(value.verified_host_names.as_string_vec());
        relay_details.unverified_host_names(
            value.unverified_host_names.as_string_vec(),
        );
        relay_details.last_restarted(value.last_restarted.to_string());
        relay_details.bandwidth_avg(value.bandwidth_avg);
        relay_details.bandwidth_burst(value.bandwidth_burst);
        relay_details.observed_bandwidth(value.bandwidth_observed);
        // TODO
        relay_details.advertised_bandwidth(None);
        relay_details.overload_general_timestamp(
            value.overload_general_timestamp.process_timestamp(),
        );
        relay_details.overload_fd_exhausted_timestamp(
            value.overload_fd_exhausted_timestamp.process_timestamp(),
        );
        relay_details.exit_policy(value.exit_policy.as_string_vec());
        // TODO
        relay_details.exit_policy_v6_summary(None);
        relay_details.contact(value.contact);
        relay_details.version(value.version);
        relay_details.platform(value.platform);
        // TODO
        relay_details.recommended_version(None);
        relay_details.version_status(value.version_status);
        relay_details.effective_family(value.effective_family.as_string_vec());
        // TODO
        relay_details.indirect_family(None);
        // TODO
        relay_details.measured(None);
        // TODO
        relay_details.unreachable_or_addresses(None);
        relay_details.last_changed_address_or_port(
            value.last_changed_address_or_port.to_string(),
        );
        relay_details.diff_or_addresses(
            value.diff_or_addresses.unwrap_or_else(|| String::from("")),
        );
        relay_details.build().unwrap()
    }
}

#[skip_serializing_none]
#[derive(
    Debug, Clone, Serialize, Deserialize, Builder, JsonSchema, ApiComponent,
)]
pub struct BridgeDetails {
    /// Bridge nickname consisting of 1-19 alphanumerical characters.
    pub nickname: String,

    /// SHA-1 hash of the bridge fingerprint consisting of 40 upper-case
    /// hexadecimal characters.
    pub hashed_fingerprint: String,

    /// Array of sanitized IPv4 or IPv6 addresses and TCP ports or port lists
    /// where the bridge accepts onion-routing connections. The first address
    /// is the primary onion-routing address that the bridge used to register
    /// in the network, subsequent addresses are in arbitrary order. IPv6 hex
    /// characters are all lower-case. Sanitized IP addresses are always in
    /// 10/8 or [fd9f:2e19:3bcf/48] IP networks and are only useful to learn
    /// which IP version the bridge uses and to detect whether the bridge
    /// changed its address. Sanitized IP addresses always change on the 1st of
    /// every month at 00:00:00 UTC, regardless of the bridge actually changing
    /// its IP address. TCP ports are sanitized in a similar way as IP
    /// addresses to hide the location of bridges with unusual TCP ports.
    pub or_addresses: Vec<String>,

    /// UTC timestamp (YYYY-MM-DD hh:mm:ss) when this bridge was last seen in a
    /// bridge network status.
    pub last_seen: String,

    /// UTC timestamp (YYYY-MM-DD hh:mm:ss) when this bridge was first seen in
    /// a bridge network status.
    pub first_seen: String,

    /// Boolean field saying whether this bridge was listed as running in the
    /// last bridge network status.
    pub running: bool,

    /// Array of relay flags that the bridge authority assigned to this bridge.
    /// May be omitted if empty.
    pub flags: Vec<String>,

    /// UTC timestamp (YYYY-MM-DD hh:mm:ss) when the bridge was last
    /// (re-)started. Missing if router descriptor containing this information
    /// cannot be found.
    pub last_restarted: String,

    /// Bandwidth in bytes per second that this bridge is willing and capable
    /// to provide. This bandwidth value is the minimum of bandwidth_avg,
    /// bandwidth_burst, and observed_bandwidth. Missing if router descriptor
    /// containing this information cannot be found.
    pub advertised_bandwidth: Option<f32>,

    /// Indicates that a relay has reached an "overloaded state" which can be
    /// one or many of the following load metrics:
    /// - Any OOM invocation due to memory pressure
    /// - Any ntor onionskins are dropped
    /// - TCP port exhaustion
    ///
    /// The timestamp is when at least one metrics was detected.
    pub overload_general_timestamp: String,

    /// Platform string containing operating system and Tor version details.
    /// Omitted if not provided by the bridge or if descriptor containing this
    /// information cannot be found.
    pub platform: Option<String>,

    /// Tor software version without leading "Tor" as reported by the bridge in
    /// the "platform" line of its server descriptor. Omitted if not provided
    /// by the bridge, if the descriptor containing this information cannot be
    /// found, or if the bridge runs an alternative Tor implementation.
    pub version: Option<String>,

    /// Boolean field saying whether the Tor software version of this bridge is
    /// recommended by the directory authorities or not. Uses the bridge
    /// version in the bridge network status. Omitted if either the directory
    /// authorities did not recommend versions, or the bridge did not report
    /// which version it runs.
    pub recommended_version: Option<bool>,

    /// Status of the Tor software version of this bridge based on the versions
    /// recommended by the directory authorities. Possible version statuses
    /// are: "recommended" if a version is listed as recommended;
    /// "experimental" if a version is newer than every recommended version;
    /// "obsolete" if a version is older than every recommended version; "new
    /// in series" if a version has other recommended versions with the same
    /// first three components, and the version is newer than all such
    /// recommended versions, but it is not newer than every recommended
    /// version; "unrecommended" if none of the above conditions hold. Omitted
    /// if either the directory authorities did not recommend versions, or the
    /// bridge did not report which version it runs.
    pub version_status: Option<String>,

    /// Array of (pluggable) transport names supported by this bridge.
    pub transports: Vec<String>,

    /// Array of country codes where this bridge is not served because we
    /// believe it to be blocked.
    pub blocklist: Vec<String>,

    /// BridgeDB distributor that this bridge is currently assigned to. Omitted
    /// if this bridge is currently not assigned to any BridgeDB distributor.
    /// Added on February 14, 2020.
    pub bridgedb_distributor: Option<String>,
}

impl From<metrics::BridgeDetailsRow> for BridgeDetails {
    fn from(value: metrics::BridgeDetailsRow) -> Self {
        let mut bridge_details = BridgeDetailsBuilder::default();
        bridge_details.nickname(value.nickname);
        bridge_details.hashed_fingerprint(value.fingerprint);
        bridge_details.or_addresses(value.or_addresses.as_string_vec());
        bridge_details.first_seen(value.first_seen.to_string());
        bridge_details.last_seen(value.last_seen.to_string());
        bridge_details.running(value.running.unwrap_or(false));
        bridge_details.flags(value.flags.as_string_vec());
        bridge_details.last_restarted(value.last_restarted.to_string());
        bridge_details.overload_general_timestamp(
            value.overload_general_timestamp.process_timestamp(),
        );
        bridge_details.platform(value.platform);
        bridge_details.version_status(value.version_status);
        bridge_details.transports(value.transport.as_string_vec());
        bridge_details.blocklist(value.blocklist.as_string_vec());
        // TODO
        bridge_details.advertised_bandwidth(None);
        bridge_details.version(value.version);
        // TODO
        bridge_details.recommended_version(None);
        bridge_details.bridgedb_distributor(value.bridgedb_distributor);
        bridge_details.build().unwrap()
    }
}

#[allow(clippy::bool_assert_comparison)]
#[cfg(test)]
mod tests {
    use sqlx::types::chrono::{self, NaiveDate};

    use super::*;
    use crate::metrics::{BridgeDetailsRow, RelayDetailsRow};

    #[test]
    fn test_from_bridge_details_row() {
        let nickname: String = "nickname".into();
        let fingerprint: String = "fingerprint".into();
        let or_addresses: Option<String> =
            Some(r#"["[ff:ffaa::ff]:2938"]"#.to_string());
        let first_seen: chrono::NaiveDateTime =
            NaiveDate::from_ymd_opt(2016, 7, 8)
                .unwrap()
                .and_hms_opt(0, 0, 0)
                .unwrap();
        let last_seen: chrono::NaiveDateTime =
            NaiveDate::from_ymd_opt(2016, 7, 8)
                .unwrap()
                .and_hms_opt(10, 0, 0)
                .unwrap();
        let running: Option<bool> = None;
        let flags: Option<String> = None;
        let last_restarted: chrono::NaiveDateTime =
            NaiveDate::from_ymd_opt(2020, 7, 8)
                .unwrap()
                .and_hms_opt(0, 0, 0)
                .unwrap();
        let overload_general_timestamp: Option<chrono::NaiveDateTime> = Some(
            NaiveDate::from_ymd_opt(2016, 7, 8)
                .unwrap()
                .and_hms_opt(10, 0, 0)
                .unwrap(),
        );
        let version: Option<String> = Some("version".into());
        let platform: Option<String> = Some("platform".into());
        let version_status: Option<String> = None;
        let transport: Option<String> = None;
        let blocklist: Option<String> = None;
        let bridgedb_distributor: Option<String> = None;

        let details_row = BridgeDetailsRow {
            nickname,
            fingerprint,
            running,
            flags,
            version,
            or_addresses,
            platform,
            first_seen,
            last_seen,
            overload_general_timestamp,
            version_status,
            transport,
            blocklist,
            last_restarted,
            bridgedb_distributor,
        };

        let bridge_details = BridgeDetails::from(details_row.clone());
        assert_eq!(bridge_details.nickname, details_row.nickname);
        assert_eq!(bridge_details.hashed_fingerprint, details_row.fingerprint);
        assert_eq!(
            bridge_details.or_addresses,
            vec!["[ff:ffaa::ff]:2938".to_string()]
        );
        assert_eq!(
            bridge_details.first_seen,
            "2016-07-08 00:00:00".to_string()
        );
        assert_eq!(
            bridge_details.last_seen,
            "2016-07-08 10:00:00".to_string()
        );
        assert_eq!(bridge_details.running, false);
        assert_eq!(bridge_details.flags, Vec::<String>::default());
        assert_eq!(
            bridge_details.last_restarted,
            "2020-07-08 00:00:00".to_string()
        );
        assert_eq!(
            bridge_details.overload_general_timestamp,
            "2016-07-08 10:00:00".to_string()
        );
        assert_eq!(bridge_details.version, details_row.version);
        assert_eq!(bridge_details.platform, details_row.platform);
        assert_eq!(bridge_details.version_status, details_row.version_status);
        assert_eq!(bridge_details.transports, Vec::<String>::default());
        assert_eq!(bridge_details.blocklist, Vec::<String>::default());
        assert_eq!(
            bridge_details.bridgedb_distributor,
            details_row.bridgedb_distributor
        );
    }

    #[test]
    fn test_from_relay_details_row() {
        let nickname: String = "nickname".into();
        let fingerprint: String = "fingerprint".into();
        let or_addresses: Option<String> =
            Some(r#"["[ff:ffaa::ff]:2938"]"#.to_string());
        let dir_port: Option<i32> = Some(0);
        let first_seen: chrono::NaiveDateTime =
            NaiveDate::from_ymd_opt(2016, 7, 8)
                .unwrap()
                .and_hms_opt(0, 0, 0)
                .unwrap();
        let last_seen: chrono::NaiveDateTime =
            NaiveDate::from_ymd_opt(2016, 7, 8)
                .unwrap()
                .and_hms_opt(10, 0, 0)
                .unwrap();
        let running: Option<bool> = None;
        let is_hibernating = Some(true);
        let country = Some("it".into());
        let country_name = Some("Italy".into());
        let autonomous_system = Some("autonomous system".into());
        let as_name = Some("as name".into());
        let flags: Option<String> = None;
        let last_restarted: chrono::NaiveDateTime =
            NaiveDate::from_ymd_opt(2020, 7, 8)
                .unwrap()
                .and_hms_opt(0, 0, 0)
                .unwrap();
        let overload_general_timestamp: Option<chrono::NaiveDateTime> = Some(
            NaiveDate::from_ymd_opt(2016, 7, 8)
                .unwrap()
                .and_hms_opt(10, 0, 0)
                .unwrap(),
        );
        let overload_fd_exhausted_timestamp: Option<chrono::NaiveDateTime> =
            Some(
                NaiveDate::from_ymd_opt(2016, 7, 8)
                    .unwrap()
                    .and_hms_opt(10, 0, 0)
                    .unwrap(),
            );
        let version: Option<String> = Some("version".into());
        let platform: Option<String> = Some("platform".into());
        let version_status: Option<String> = None;
        let verified_host_names = Some("".into());
        let unverified_host_names = Some("".into());
        let bandwidth_avg = Some(10);
        let bandwidth_burst = Some(100);
        let bandwidth_observed = Some(1000);
        let advertised_bandwidth = Some(1000);
        let bandwidth = Some(1000);
        let network_weight_fraction = Some(0.000001);
        let family_digest = Some("AAAAAAAA".into());
        let effective_family = Some("[]".into());
        let declared_family = Some("[]".into());
        let exit_policy = Some(r#"["reject *:*"]"#.into());
        let contact = Some("contact".into());
        let ipv6_default_policy = Some(r#""accept""#.into());
        let last_changed_address_or_port: chrono::NaiveDateTime =
            NaiveDate::from_ymd_opt(2016, 7, 8)
                .unwrap()
                .and_hms_opt(0, 0, 0)
                .unwrap();
        let diff_or_addresses = Some("".into());
        let unreachable_or_addresses = Some("".into());
        let overload_ratelimits_timestamp: Option<chrono::NaiveDateTime> =
            Some(
                NaiveDate::from_ymd_opt(2016, 7, 8)
                    .unwrap()
                    .and_hms_opt(10, 0, 0)
                    .unwrap(),
            );
        let overload_ratelimits_ratelimit = Some(1000);
        let overload_ratelimits_burstlimit = Some(1000);
        let overload_ratelimits_read_count = Some(1000);
        let overload_ratelimits_write_count = Some(1000);

        let relay_details_row = RelayDetailsRow {
            nickname,
            fingerprint,
            or_addresses,
            last_seen,
            first_seen,
            running,
            flags,
            country,
            country_name,
            autonomous_system,
            as_name,
            verified_host_names,
            last_restarted,
            exit_policy,
            contact,
            platform,
            version,
            version_status,
            effective_family,
            declared_family,
            last_changed_address_or_port,
            diff_or_addresses,
            unverified_host_names,
            unreachable_or_addresses,
            overload_ratelimits_timestamp,
            overload_ratelimits_ratelimit,
            overload_ratelimits_burstlimit,
            overload_ratelimits_read_count,
            overload_ratelimits_write_count,
            overload_fd_exhausted_timestamp,
            overload_general_timestamp,
            family_digest,
            advertised_bandwidth,
            bandwidth,
            network_weight_fraction,
            bandwidth_avg,
            bandwidth_burst,
            bandwidth_observed,
            ipv6_default_policy,
            dir_port,
            is_hibernating,
        };

        let relay_details = RelayDetails::from(relay_details_row.clone());

        assert_eq!(relay_details.nickname, relay_details_row.nickname);
        assert_eq!(relay_details.fingerprint, relay_details_row.fingerprint);
        assert_eq!(
            relay_details.or_addresses,
            vec!["[ff:ffaa::ff]:2938".to_string()]
        );
        assert_eq!(
            relay_details.dir_address,
            relay_details_row.dir_port.unwrap_or(0).to_string()
        );
        assert_eq!(
            relay_details.first_seen,
            "2016-07-08 00:00:00".to_string()
        );
        assert_eq!(relay_details.last_seen, "2016-07-08 10:00:00".to_string());
        assert_eq!(relay_details.running, false);
        assert_eq!(relay_details.flags, Vec::<String>::default());
        assert_eq!(
            relay_details.last_changed_address_or_port,
            "2016-07-08 00:00:00".to_string()
        );
        assert_eq!(
            relay_details.hibernating,
            relay_details_row.is_hibernating
        );
        assert_eq!(relay_details.country_name, relay_details_row.country_name);
        assert_eq!(relay_details.country, relay_details_row.country);
        assert_eq!(relay_details.r#as, relay_details_row.autonomous_system);
        assert_eq!(relay_details.as_name, relay_details_row.as_name);
        assert_eq!(relay_details.alleged_family, None);
        assert_eq!(
            relay_details.verified_host_names,
            Vec::<String>::default()
        );
        assert_eq!(
            relay_details.unverified_host_names,
            Vec::<String>::default()
        );
        assert_eq!(
            relay_details.last_restarted,
            "2020-07-08 00:00:00".to_string()
        );
        assert_eq!(
            relay_details.bandwidth_avg,
            relay_details_row.bandwidth_avg
        );
        assert_eq!(
            relay_details.bandwidth_burst,
            relay_details_row.bandwidth_burst
        );
        assert_eq!(relay_details.advertised_bandwidth, None);
        assert_eq!(
            relay_details.observed_bandwidth,
            relay_details_row.bandwidth_observed
        );
        assert_eq!(
            relay_details.overload_general_timestamp,
            "2016-07-08 10:00:00".to_string()
        );
        assert_eq!(relay_details.exit_policy, vec!["reject *:*"]);
        assert_eq!(relay_details.exit_policy_v6_summary, None);
        assert_eq!(relay_details.contact, relay_details_row.contact);
        assert_eq!(relay_details.version, relay_details_row.version);
        assert_eq!(relay_details.platform, relay_details_row.platform);
        assert_eq!(relay_details.recommended_version, None);
        assert_eq!(
            relay_details.version_status,
            relay_details_row.version_status
        );
        assert_eq!(relay_details.effective_family, Vec::<String>::default());
        assert_eq!(relay_details.indirect_family, None);
        assert_eq!(relay_details.measured, None);
        assert_eq!(relay_details.unreachable_or_addresses, None);
    }
}
