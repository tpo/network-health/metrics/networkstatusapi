use super::responses::generic::ResponseBuilder;

#[derive(Debug)]
pub struct ResponseFactory;

impl Default for ResponseFactory {
    fn default() -> Self {
        Self::new()
    }
}

impl ResponseFactory {
    pub fn new() -> Self {
        Self
    }

    pub fn get<R, B>(&self) -> ResponseBuilder<R, B>
    where
        R: Clone,
        B: Clone,
    {
        let mut builder = ResponseBuilder::default();
        builder.version(env!("NSAPI_PROTOCOL_VERSION"));
        builder.build_version(option_env!("NSAPI_BUILD_GIT_HASH"));
        builder.next_major_version_scheduled(None);

        builder
    }
}
