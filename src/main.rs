use std::{env, net::TcpListener};

use dotenv::dotenv;
use network_status_api::{
    models::factory::ResponseFactory, server::run,
    victoriametrics::proxy::VictoriaMetricsProxyConfig,
};
use sqlx::PgPool;

#[tokio::main]
async fn main() -> Result<(), std::io::Error> {
    dotenv().ok();

    let db_url = env::var("DATABASE_URL")
        .expect("could not find DATABASE_URL in envs.");
    let connection = PgPool::connect(&db_url)
        .await
        .expect("cannot connect to Pg.");

    let factory = ResponseFactory::new();

    let vm_auth =
        env::var("VM_PASSWORD").expect("could not find VM_PASSWORD in envs.");
    let vm_user =
        env::var("VM_USERNAME").expect("could not find VM_USERNAME in envs.");
    let vm_url = env::var("VM_URL").expect("could not find VM_URL in envs.");
    let vm_config = VictoriaMetricsProxyConfig::with(vm_url, vm_user, vm_auth);

    let address = format!("{}:{}", "0.0.0.0", "3000");

    let listener = TcpListener::bind(address)?;
    run(listener, connection, factory, vm_config)?.await
}
