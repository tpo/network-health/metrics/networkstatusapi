use std::{
    net::TcpListener,
    sync::{Arc, RwLock},
};

use actix_web::{dev::Server, middleware, web, App, HttpServer};
use apistos::{
    app::{BuildConfig, OpenApiWrapper},
    info::Info,
    server::Server as apistosServer,
    spec::Spec,
    web::{get, resource},
    RapidocConfig, RedocConfig, ScalarConfig, SwaggerUIConfig,
};
use sqlx::Pool;

use crate::{
    handlers::*, models::factory::ResponseFactory, victoriametrics::proxy::*,
};

#[allow(clippy::arc_with_non_send_sync)]
pub fn run<D: sqlx::Database>(
    listener: TcpListener,
    db_pool: Pool<D>,
    factory: ResponseFactory,
    vm_auth: VictoriaMetricsProxyConfig,
) -> Result<Server, std::io::Error> {
    let factory = Arc::new(RwLock::new(factory));

    std::env::set_var("RUST_LOG", "actix_web=info");
    let _ = env_logger::try_init();

    let server = HttpServer::new(move || {
        let vm_proxy = Arc::new(VictoriaMetricsProxy::with(&vm_auth));

        let spec = Spec {
            info: Info {
                title: "A well documented API".to_string(),
                ..Default::default()
            },
            servers: vec![apistosServer {
                url: "/".to_string(),
                ..Default::default()
            }],
            ..Default::default()
        };
        App::new()
            .document(spec)
            .wrap(middleware::Logger::new(
                r#"{"status":"%s","header":"%r","size":"%b","tts":%T}"#,
            ))
            .wrap(middleware::Compress::default())
            .app_data(web::Data::new(db_pool.clone()))
            .app_data(web::Data::new(factory.clone()))
            .app_data(web::Data::new(vm_proxy.clone()))
            .service(resource("/_meta").route(get().to(meta)))
            .service(resource("/health").route(get().to(health)))
            .service(resource("/summary").route(get().to(get_summary)))
            .service(resource("/details").route(get().to(get_details)))
            .service(resource("/bandwidth").route(get().to(get_bandwidth)))
            .service(resource("/weights").route(get().to(get_weights)))
            .service(resource("/clients").route(get().to(get_clients)))
            .service(
                resource("/exit_addresses").route(get().to(get_exit_node)),
            )
            .service(
                resource("/weights_probability")
                    .route(get().to(get_weights_probability)),
            )
            .build_with(
                "/openapi.json",
                BuildConfig::default()
                    .with(RapidocConfig::new(&"/rapidoc"))
                    .with(RedocConfig::new(&"/redoc"))
                    .with(ScalarConfig::new(&"/scalar"))
                    .with(SwaggerUIConfig::new(&"/swagger")),
            )
    })
    .listen(listener)?
    .run();

    Ok(server)
}
