fn main() {
    set_protocol_version();
    set_revision_hash();
}

fn set_protocol_version() {
    let protocol_version = "0.1";

    println!(
        "cargo:rustc-env=NSAPI_PROTOCOL_VERSION={}",
        protocol_version
    );
}

fn set_revision_hash() {
    use std::process::Command;

    let args = &["rev-parse", "--short=10", "HEAD"];
    let Ok(output) = Command::new("git").args(args).output() else {
        return;
    };

    let rev = String::from_utf8_lossy(&output.stdout).trim().to_string();
    if rev.is_empty() {
        return;
    }

    println!("cargo:rustc-env=NSAPI_BUILD_GIT_HASH={}", rev);
}
