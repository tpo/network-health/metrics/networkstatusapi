use actix_web::dev;
use awc::ClientResponse;
use network_status_api::test_utils::spawn_app;

async fn make_weights_req(
    params: Vec<(&str, &str)>,
) -> ClientResponse<dev::Decompress<dev::Payload>> {
    let app = spawn_app().await;
    let client = awc::Client::new();

    client
        .get(&format!("{}/weights", app.addr))
        .query(&params)
        .unwrap()
        .send()
        .await
        .unwrap()
}

#[actix_web::test]
async fn weights_test_invalid_lookup() {
    let req = make_weights_req(vec![("lookup", "invalid")]).await;
    assert_eq!(req.status().as_str(), "400");
}

#[actix_web::test]
async fn weights_test_invalid_type() {
    let req = make_weights_req(vec![("type", "non_existent_type")]).await;
    assert_eq!(req.status().as_str(), "400");
}
