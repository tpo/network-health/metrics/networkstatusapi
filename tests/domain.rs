use network_status_api::models::query::{
    domain::{
        CaseInsensitiveString, CountryCode, DaysAgoRange, ParametersType,
        PositiveOrZeroValue, Version, VersionType,
    },
    params::{QueryFilters, QueryParamsBuilder},
};

#[test]
fn test_empty_case_insensitive_string() {
    assert!(
        CaseInsensitiveString::try_from("".to_string()).is_err(),
        "case insensitive string can't be empty"
    );
}

#[test]
fn test_non_empty_case_insensitive_string() {
    assert!(CaseInsensitiveString::try_from("non-empty".to_string()).is_ok());
}

#[test]
fn test_case_insensitive_string() {
    let some_string = "SomE CasEiNsensitiVe String".to_string();
    assert_eq!(
        "some caseinsensitive string",
        CaseInsensitiveString::try_from(some_string)
            .unwrap()
            .as_ref()
    )
}

#[test]
fn test_country_region_code() {
    assert!(
        CountryCode::try_from("A".to_string()).is_err(),
        "country code should be 2 chars long"
    );
    assert!(
        CountryCode::try_from("ABC".to_string()).is_err(),
        "country code should be 2 chars long"
    );
    assert!(CountryCode::try_from("AB".to_string()).is_ok());
}

#[test]
fn test_try_from_datatype() {
    assert_eq!(
        ParametersType::Bridge,
        ParametersType::try_from("bridge".to_string()).unwrap()
    );
    assert_eq!(
        ParametersType::Relay,
        ParametersType::try_from("relay".to_string()).unwrap()
    );
    assert_eq!(None, ParametersType::try_from("other".to_string()).ok());
}

#[test]
fn test_zero_or_positive_value() {
    assert_eq!(
        &0,
        PositiveOrZeroValue::from(-12).as_ref(),
        "negative value cannot be different than zero"
    );
    assert_eq!(&0, PositiveOrZeroValue::from(0).as_ref());
    assert_eq!(&113, PositiveOrZeroValue::from(113).as_ref());
}

#[test]
fn test_fields() {
    let invalid_field = QueryParamsBuilder::default()
        .fields("nickname,created_date,as".into())
        .build()
        .unwrap();

    assert_eq!(
        QueryFilters::try_from(invalid_field).err().unwrap(),
        r#""created_date" is not a valid field."#
    );

    let invalid_field = QueryParamsBuilder::default()
        .fields("nickname, as".into())
        .build()
        .unwrap();

    let valid_field = QueryParamsBuilder::default()
        .fields("nickname,fingerprint,as".into())
        .build()
        .unwrap();

    assert_eq!(
        QueryFilters::try_from(invalid_field).err().unwrap(),
        r#"" as" is not a valid field."#,
        "whitespaces should not be accepted in fields param."
    );
    assert!(QueryFilters::try_from(valid_field).is_ok());
}

#[test]
fn test_query_filters() {
    let invalid_cc = QueryParamsBuilder::default()
        .country("ITA".into())
        .build()
        .unwrap();

    let invalid_lookup_short = QueryParamsBuilder::default()
        .lookup("somehash".to_string())
        .build()
        .unwrap();

    let invalid_lookup_long = QueryParamsBuilder::default()
        .lookup("s".repeat(41))
        .build()
        .unwrap();

    let invalid_params_type = QueryParamsBuilder::default()
        .r#type("roof".to_string())
        .build()
        .unwrap();

    assert!(QueryFilters::try_from(invalid_cc).is_err());
    assert!(QueryFilters::try_from(invalid_lookup_short).is_err());
    assert!(QueryFilters::try_from(invalid_lookup_long).is_err());
    assert!(QueryFilters::try_from(invalid_params_type).is_err());
}

#[test]
fn test_version_type() {
    let invalid = vec![
        "1.0.1,1",
        "1.2.2-1.0.0.1",
        "1.23.3.3...12.2.2.2",
        "1.2.3.5..183.9.9.9.0",
    ];
    for v in invalid {
        let version = QueryParamsBuilder::default()
            .version(v.into())
            .build()
            .unwrap();

        assert!(QueryFilters::try_from(version).is_err());
    }

    let valid = vec![
        "1.0.1.1..2.1.0.2",
        "1.0.1.1,1.1.3.3-alpha,1.3.3.3-dev",
        "2.10.1.3-alpha-dev",
    ];
    let expected = [
        VersionType::Range(
            Version::try_from("1.0.1.1".to_string()).unwrap(),
            Version::try_from("2.1.0.2".to_string()).unwrap(),
        ),
        VersionType::List(vec![
            Version::try_from("1.0.1.1".to_string()).unwrap(),
            Version::try_from("1.1.3.3-alpha".to_string()).unwrap(),
            Version::try_from("1.3.3.3-dev".to_string()).unwrap(),
        ]),
        VersionType::Value(
            Version::try_from("2.10.1.3-alpha-dev".to_string()).unwrap(),
        ),
    ];

    for (i, v) in valid.into_iter().enumerate() {
        let version = QueryParamsBuilder::default()
            .version(v.into())
            .build()
            .unwrap();

        let filters = QueryFilters::try_from(version).unwrap();
        assert_eq!(expected[i], filters.version.unwrap());
    }
}

#[test]
fn test_version() {
    let v1 = Version::try_from("1.0.2.2-alpha".to_string());
    let v2 = Version::try_from("1.0.2.2-rc".to_string());
    let v3 = Version::try_from("1.0.2.2-dev".to_string());
    let v4 = Version::try_from("1.9.9.1".to_string());
    let v5 = Version::try_from("1.9.9.1a-alpha".to_string());
    let v6 = Version::try_from("1.9.9-alpha".to_string());
    assert!(v1.is_ok());

    let v1 = v1.unwrap();
    assert_eq!(v1.cvs, Some("-alpha".to_string()));
    assert_eq!(v1.major, 1);
    assert_eq!(v1.minor, 0);
    assert_eq!(v1.micro, 2);
    assert_eq!(v1.patchlevel, 2);

    assert!(v2.is_ok());
    assert!(v3.is_ok());
    assert!(v4.is_ok());

    let v4 = v4.unwrap();
    assert_eq!(v4.cvs, None);
    assert_eq!(v4.major, 1);
    assert_eq!(v4.minor, 9);
    assert_eq!(v4.micro, 9);
    assert_eq!(v4.patchlevel, 1);

    assert!(v5.is_err());
    assert!(v6.is_err());
}

#[test]
fn test_days_ago_range() {
    let invalid = vec!["1-2-3", "--2", "", "10+", "2--"];
    for i in invalid {
        let invalid_range = QueryParamsBuilder::default()
            .last_seen_days(i.into())
            .build()
            .unwrap();

        assert!(QueryFilters::try_from(invalid_range).is_err());
    }

    let valid = vec!["3", "-3", "1-"];
    let expected = [
        DaysAgoRange::Range(3, 3),
        DaysAgoRange::AtMost(3),
        DaysAgoRange::AtLeast(1),
    ];
    for (i, v) in valid.into_iter().enumerate() {
        let valid_range = QueryParamsBuilder::default()
            .last_seen_days(v.into())
            .build()
            .unwrap();

        let filters = QueryFilters::try_from(valid_range).unwrap();
        assert_eq!(expected[i], filters.last_seen_days.unwrap());
    }
}
