use actix_web::dev;
use awc::ClientResponse;
use network_status_api::test_utils::spawn_app;

async fn make_clients_req(
    params: Vec<(&str, &str)>,
) -> ClientResponse<dev::Decompress<dev::Payload>> {
    let app = spawn_app().await;
    let client = awc::Client::new();

    client
        .get(&format!("{}/clients", app.addr))
        .query(&params)
        .unwrap()
        .send()
        .await
        .unwrap()
}

#[actix_web::test]
async fn clients_test_no_lookup() {
    let req = make_clients_req(vec![("lookup", "invalid")]).await;
    assert_eq!(req.status().as_str(), "400");
}
