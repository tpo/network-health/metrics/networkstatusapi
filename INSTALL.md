# Network Status APIs | Installation Guide

Welcome to Network Status APIs! This guide will teach you how you to host your
very own instance of Network Status APIs.

## What you need
The only thing that is required to compile the project is Rust 1.68+, you can
install it by following the [official
guide](https://www.rust-lang.org/learn/get-started) on Rust's webiste

## Setup

The binary expects to find some `env` variables at runtime, without those the
program will panic.  You can find the necessary envs in the `.env.example` file
in the project folder.

Create an `.env` file that will contain the needed variables to run the service
locally:

```sh
$ cat .env.example > .env
```

You may need to contact the Tor admins if you need access to some of the
variables needed in `.env` file.

## Run with cargo
After Rust is installed on your machine, you simply need to run
```sh
$ cargo run --release
```

## Build from source
If you want to build the program from source, you can run

```sh
$ cargo build --release
```

This will install all the dependencies needed to compile the project's binary,
after compilation completes successfully you'll find the binary under
`/target/release`.

Finally, you can run the binary by simply running the binary
```sh
$ ./target/release/network-status-api
```
