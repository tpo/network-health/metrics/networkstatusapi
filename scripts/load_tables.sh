#!/usr/bin/env bash
set -x
set -eo pipefail

# Check if a custom user has been set, otherwise default to 'postgres'
DB_USER=${POSTGRES_USER:=metrics}
# Check if a custom password has been set, otherwise default to 'password'
DB_PASSWORD="${POSTGRES_PASSWORD:=password}"
# Check if a custom database name has been set, otherwise default to 'newsletter'
DB_NAME="${POSTGRES_DB:=metrics}"
# Check if a custom port has been set, otherwise default to '5432'
DB_PORT="${POSTGRES_PORT:=5432}"
# Check if a custom host has been set, otherwise default to 'localhost'
DB_HOST="${POSTGRES_HOST:=localhost}"

export PGPASSWORD="${DB_PASSWORD}"
export DATABASE_URL=postgres://${DB_USER}:${DB_PASSWORD}@${DB_HOST}:${DB_PORT}/${DB_NAME}

files=(
    "bandwidth_tables.sql"
    "bridge_network_status_tables.sql"
    "bridge_pool_assignment_tables.sql"
    "bridgedb_metrics_tables.sql"
    "bridgestrap_tables.sql"
    "certs_tables.sql"
    "exit_lists_tables.sql"
    "extra_info_tables.sql"
    "network_status_tables.sql"
    "onionperf_tables.sql"
    "server_tables.sql"
    "status_tables.sql"
)

for f in "${files[@]}"
do
  psql -h "${DB_HOST}" -U "${DB_USER}" -p "${DB_PORT}" -d "metrics" -a -f "tables/${f}"
done
