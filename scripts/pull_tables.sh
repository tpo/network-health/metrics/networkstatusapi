#!/usr/bin/env bash
set -eo pipefail

files=(
    "bandwidth_tables.sql"
    "bridge_network_status_tables.sql"
    "bridge_pool_assignment_tables.sql"
    "bridgedb_metrics_tables.sql"
    "bridgestrap_tables.sql"
    "certs_tables.sql"
    "exit_lists_tables.sql"
    "extra_info_tables.sql"
    "network_status_tables.sql"
    "onionperf_tables.sql"
    "server_tables.sql"
    "status_tables.sql"
)

mkdir -p "tables"

for f in "${files[@]}"
do
  url="https://gitlab.torproject.org/tpo/network-health/metrics/descriptorParser/-/raw/main/src/main/sql/${f}"
  curl -so "tables/$f" "$url"

  if [ $? -eq 0 ]; then
    echo "Download of $f completed successfully."
  else
    echo "Download of $f failed."
  fi
done
