# Network Status API Documentation

#### Get relay/bridges summary

Summary documents contain short summaries of relays with nicknames, fingerprints, IP addresses, and running information as well as bridges with hashed fingerprints and running information.

<details>
 <summary><code>GET</code> <code><b>/summary</b></code> <code>Get relays/bridges summary</code></summary>

##### Parameters

> | Name                 |  Type     | Status     | Data type               | Description                      |
> |----------------------|-----------|---------------|------------------------|----------------------------------|
> | type                 |  optional | Available | string | Return only relay (parameter value relay) or only bridge documents (parameter value bridge). Parameter values are case-insensitive.  |
> | running              |  optional | Available  | string | Return only running (parameter value true) or only non-running relays and/or bridges (parameter value false). Parameter values are case-insensitive.  |
> | search               |  optional | Not Available  | string | Return only (1) relays with the parameter value matching (part of a) nickname, (possibly $-prefixed) beginning of a hex-encoded fingerprint, any 4 hex character block of a space-separated fingerprint, beginning of a base64-encoded fingerprint without trailing equal signs, or beginning of an IP address (possibly enclosed in square brackets in case of IPv6), (2) bridges with (part of a) nickname or (possibly $prefixed) beginning of a hashed hex-encoded fingerprint, and (3) relays and/or bridges matching a given qualified search term. Searches by relay IP address include all known addresses used for onion routing and for exiting to the Internet. Searches for beginnings of IP addresses are performed on textual representations of canonical IP address forms, so that searches using CIDR notation or non-canonical forms will return empty results. Searches are case-insensitive, except for base64-encoded fingerprints. If multiple search terms are given, separated by spaces, the intersection of all relays and bridges matching all search terms will be returned. Complete hex-encoded fingerprints should always be hashed using SHA-1, regardless of searching for a relay or a bridge, in order to not accidentally leak non-hashed bridge fingerprints in the URL. Qualified search terms have the form "key:value" (without double quotes) with "key" being one of the parameters listed here except for "search", "fingerprint", "order", "limit", "offset", and "fields", and "value" being the string that will internally be passed to that parameter. If a qualified search term for a given "key" is specified more than once, only the first "value" is considered.  |
> | lookup               |  optional | Available  | string | Return only the relay with the parameter value matching the fingerprint or the bridge with the parameter value matching the hashed fingerprint. Fingerprints should always be hashed using SHA-1, regardless of looking up a relay or a bridge, in order to not accidentally leak non-hashed bridge fingerprints in the URL. Lookups only work for full fingerprints or hashed fingerprints consisting of 40 hex characters. Lookups are case-insensitive.  |
> | country              |  optional | Available  | string | Return only relays which are located in the given country as identified by a two-letter country code. Filtering by country code is case-insensitive. The special country code xz can be used for relays that were not found in the GeoIP database.  |
> | as                   |  optional | Available  | string | Return only relays which are located in either one of the given autonomous systems (AS) as identified by AS number (with or without preceding "AS" part). Multiple AS numbers can be provided separated by commas. Filtering by AS number is case-insensitive. The special AS number 0 can be used for relays that were not found in the GeoIP database.  |
> | as_name              |  optional | Available  | string | Return only relays with the parameter value matching (part of) the autonomous system (AS) name they are located in. If the parameter value contains spaces, only relays are returned which contain all space-separated parts in their AS name. Only printable ASCII characters are permitted in the parameter value, some of which need to be percent-encoded (# as %23, % as %25, & as %26, + as %2B, and / as %2F). Comparisons are case-insensitive.  |
> | flag                 |  optional | Not Available  | string | Return only relays which have the given relay flag assigned by the directory authorities. Note that if the flag parameter is specified more than once, only the first parameter value will be considered. Filtering by flag is case-insensitive.  |
> | first_seen_days      |  optional | Not Available  | string | Return only relays or bridges which have first been seen during the given range of days ago. A parameter value "x-y" with x <= y returns relays or bridges that have first been seen at least x and at most y days ago. Accepted short forms are "x", "x-", and "-y" which are interpreted as "x-x", "x-infinity", and "0-y".  |
> | last_seen_days       |  optional | Not Available  | string | Return only relays or bridges which have last been seen during the given range of days ago. A parameter value "x-y" with x <= y returns relays or bridges that have last been seen at least x and at most y days ago. Accepted short forms are "x", "x-", and "-y" which are interpreted as "x-x", "x-infinity", and "0-y". Note that relays and bridges that haven't been running in the past week are not included in results, so that setting x to 8 or higher will lead to an empty result set.  |
> | first_seen_since     |  optional | Available  | string | Return only relays or bridges which have first been seen after the given date. The date has to be passed in the format "yyyy-MM-dd".  |
> | last_seen_since      |  optional | Available  | string | Return only relays or bridges which have last been seen after the given date. The date has to be passed in the format "yyyy-MM-dd". Note that relays and bridges that haven't been running in the past week are not included in results, so that setting x to 8 or higher will lead to an empty result set.  |
> | contact              |  optional | Not Available  | string | Return only relays with the parameter value matching (part of) the contact line. If the parameter value contains spaces, only relays are returned which contain all space-separated parts in their contact line. Only printable ASCII characters are permitted in the parameter value, some of which need to be percent-encoded (# as %23, % as %25, & as %26, + as %2B, and / as %2F). Comparisons are case-insensitive.  |
> | family               |  optional | Available  | string | Return only the relay whose fingerprint matches the parameter value and all relays that this relay has listed in its family by fingerprint and that in turn have listed this relay in their family by fingerprint. If relays have listed other relays in their family by nickname, those family relationships will not be considered, regardless of whether they have the Named flag or not. The provided relay fingerprint must consist of 40 hex characters where case does not matter, and it must not be hashed using SHA-1. Bridges are not contained in the result, regardless of whether they define a family.  |
> | version              |  optional | Not Available  | string | Return only relays or bridges running either Tor version from a list or range given in the parameter value. Tor versions must be provided without the leading "Tor" part. Multiple versions can either be provided as a comma-separated list (","), as a range separated by two dots (".."), or as a list of ranges. Provided versions are parsed and matched by parsed dotted numbers, rather than by string prefix.  |
> | os                   |  optional | Not Available  | string | Return only relays or bridges running on an operating system that starts with the parameter value. Searches are case-insensitive.  |
> | host_name            |  optional | Not Available  | string | Return only relays with a domain name ending in the given (partial) host name. Searches for subdomains of a specific domain should ideally be prefixed with a period, for example: ".csail.mit.edu". Non-ASCII host name characters must be encoded as punycode. Filtering by host name is case-insensitive.  |
> | recommended_version  |  optional | Not Available  | string | Return only relays and bridges running a Tor software version that is recommended (parameter value true) or not recommended by the directory authorities (parameter value false). Uses the version in the consensus or bridge network status. Relays and bridges are not contained in either result, if the version they are running is not known. Parameter values are case-insensitive.  |
> | fields               |  optional | Not Available  | string | Comma-separated list of fields that will be included in the result. So far, only top-level fields in relay or bridge objects of details documents can be specified, e.g., nickname,hashed_fingerprint. If the fields parameter is provided, all other fields which are not contained in the provided list will be removed from the result. Field names are case-insensitive.  |
> | order                |  optional | Not Available  | string | Re-order results by a comma-separated list of fields in ascending or descending order. Results are first ordered by the first list element, then by the second, and so on. Possible fields for ordering are: consensus_weight and first_seen. Field names are case-insensitive. Ascending order is the default; descending order is selected by prepending fields with a minus sign (-). Field names can be listed at most once in either ascending or descending order. Relays or bridges which don't have any value for a field to be ordered by are always appended to the end, regardless or sorting order. The ordering is defined independent of the requested document type and does not require the ordering field to be contained in the document. If no order parameter is given, ordering of results is undefined.  |
> | offset               |  optional | Not Available  | string | Skip the given number of relays and/or bridges. Relays are skipped first, then bridges. Non-positive offset values are treated as zero and don't change the result.  |
> | limit                |  optional | Available  | string | Limit result to the given number of relays and/or bridges. Relays are kept first, then bridges. Non-positive limit values are treated as zero and lead to an empty result. When used together with offset, the offsetting step precedes the limiting step.  |


##### Responses

> | HTTP Code     | Content-Type                      | Response                                                            |
> |---------------|-----------------------------------|---------------------------------------------------------------------|
> | `200`         | `application/json`                | `{...}`                                |
> | `500`         | `text/html;charset=utf-8`         | None                                                                |

##### Example cURL

> ```js
>  curl -X GET -H "Content-Type: application/json" http://localhost:8080/summary
> ```

</details>


#### Get relay/bridges details

Details documents are based on network statuses published by the Tor directories, server descriptors published by relays and bridges, and data published by Tor network services TorDNSEL and BridgeDB. Details documents use the most recently published data from these sources, which may lead to contradictions between fields based on different sources in rare edge cases.

<details>
 <summary><code>GET</code> <code><b>/details</b></code> <code>Get relays/bridges details</code></summary>

##### Parameters

> | Name                 |  Type     | Status     | Data type               | Description                      |
> |----------------------|-----------|---------------|------------------------|----------------------------------|
> | type                 |  optional | Available | string | Return only relay (parameter value relay) or only bridge documents (parameter value bridge). Parameter values are case-insensitive.  |
> | running              |  optional | Available  | string | Return only running (parameter value true) or only non-running relays and/or bridges (parameter value false). Parameter values are case-insensitive.  |
> | search               |  optional | Not Available  | string | Return only (1) relays with the parameter value matching (part of a) nickname, (possibly $-prefixed) beginning of a hex-encoded fingerprint, any 4 hex character block of a space-separated fingerprint, beginning of a base64-encoded fingerprint without trailing equal signs, or beginning of an IP address (possibly enclosed in square brackets in case of IPv6), (2) bridges with (part of a) nickname or (possibly $prefixed) beginning of a hashed hex-encoded fingerprint, and (3) relays and/or bridges matching a given qualified search term. Searches by relay IP address include all known addresses used for onion routing and for exiting to the Internet. Searches for beginnings of IP addresses are performed on textual representations of canonical IP address forms, so that searches using CIDR notation or non-canonical forms will return empty results. Searches are case-insensitive, except for base64-encoded fingerprints. If multiple search terms are given, separated by spaces, the intersection of all relays and bridges matching all search terms will be returned. Complete hex-encoded fingerprints should always be hashed using SHA-1, regardless of searching for a relay or a bridge, in order to not accidentally leak non-hashed bridge fingerprints in the URL. Qualified search terms have the form "key:value" (without double quotes) with "key" being one of the parameters listed here except for "search", "fingerprint", "order", "limit", "offset", and "fields", and "value" being the string that will internally be passed to that parameter. If a qualified search term for a given "key" is specified more than once, only the first "value" is considered.  |
> | lookup               |  optional | Available  | string | Return only the relay with the parameter value matching the fingerprint or the bridge with the parameter value matching the hashed fingerprint. Fingerprints should always be hashed using SHA-1, regardless of looking up a relay or a bridge, in order to not accidentally leak non-hashed bridge fingerprints in the URL. Lookups only work for full fingerprints or hashed fingerprints consisting of 40 hex characters. Lookups are case-insensitive.  |
> | country              |  optional | Available  | string | Return only relays which are located in the given country as identified by a two-letter country code. Filtering by country code is case-insensitive. The special country code xz can be used for relays that were not found in the GeoIP database.  |
> | as                   |  optional | Available  | string | Return only relays which are located in either one of the given autonomous systems (AS) as identified by AS number (with or without preceding "AS" part). Multiple AS numbers can be provided separated by commas. Filtering by AS number is case-insensitive. The special AS number 0 can be used for relays that were not found in the GeoIP database.  |
> | as_name              |  optional | Available  | string | Return only relays with the parameter value matching (part of) the autonomous system (AS) name they are located in. If the parameter value contains spaces, only relays are returned which contain all space-separated parts in their AS name. Only printable ASCII characters are permitted in the parameter value, some of which need to be percent-encoded (# as %23, % as %25, & as %26, + as %2B, and / as %2F). Comparisons are case-insensitive.  |
> | flag                 |  optional | Not Available  | string | Return only relays which have the given relay flag assigned by the directory authorities. Note that if the flag parameter is specified more than once, only the first parameter value will be considered. Filtering by flag is case-insensitive.  |
> | first_seen_days      |  optional | Not Available  | string | Return only relays or bridges which have first been seen during the given range of days ago. A parameter value "x-y" with x <= y returns relays or bridges that have first been seen at least x and at most y days ago. Accepted short forms are "x", "x-", and "-y" which are interpreted as "x-x", "x-infinity", and "0-y".  |
> | last_seen_days       |  optional | Not Available  | string | Return only relays or bridges which have last been seen during the given range of days ago. A parameter value "x-y" with x <= y returns relays or bridges that have last been seen at least x and at most y days ago. Accepted short forms are "x", "x-", and "-y" which are interpreted as "x-x", "x-infinity", and "0-y". Note that relays and bridges that haven't been running in the past week are not included in results, so that setting x to 8 or higher will lead to an empty result set.  |
> | first_seen_since     |  optional | Available  | string | Return only relays or bridges which have first been seen after the given date. The date has to be passed in the format "yyyy-MM-dd".  |
> | last_seen_since      |  optional | Available  | string | Return only relays or bridges which have last been seen after the given date. The date has to be passed in the format "yyyy-MM-dd". Note that relays and bridges that haven't been running in the past week are not included in results, so that setting x to 8 or higher will lead to an empty result set.  |
> | contact              |  optional | Not Available  | string | Return only relays with the parameter value matching (part of) the contact line. If the parameter value contains spaces, only relays are returned which contain all space-separated parts in their contact line. Only printable ASCII characters are permitted in the parameter value, some of which need to be percent-encoded (# as %23, % as %25, & as %26, + as %2B, and / as %2F). Comparisons are case-insensitive.  |
> | family               |  optional | Available  | string | Return only the relay whose fingerprint matches the parameter value and all relays that this relay has listed in its family by fingerprint and that in turn have listed this relay in their family by fingerprint. If relays have listed other relays in their family by nickname, those family relationships will not be considered, regardless of whether they have the Named flag or not. The provided relay fingerprint must consist of 40 hex characters where case does not matter, and it must not be hashed using SHA-1. Bridges are not contained in the result, regardless of whether they define a family.  |
> | version              |  optional | Not Available  | string | Return only relays or bridges running either Tor version from a list or range given in the parameter value. Tor versions must be provided without the leading "Tor" part. Multiple versions can either be provided as a comma-separated list (","), as a range separated by two dots (".."), or as a list of ranges. Provided versions are parsed and matched by parsed dotted numbers, rather than by string prefix.  |
> | os                   |  optional | Not Available  | string | Return only relays or bridges running on an operating system that starts with the parameter value. Searches are case-insensitive.  |
> | host_name            |  optional | Not Available  | string | Return only relays with a domain name ending in the given (partial) host name. Searches for subdomains of a specific domain should ideally be prefixed with a period, for example: ".csail.mit.edu". Non-ASCII host name characters must be encoded as punycode. Filtering by host name is case-insensitive.  |
> | recommended_version  |  optional | Not Available  | string | Return only relays and bridges running a Tor software version that is recommended (parameter value true) or not recommended by the directory authorities (parameter value false). Uses the version in the consensus or bridge network status. Relays and bridges are not contained in either result, if the version they are running is not known. Parameter values are case-insensitive.  |
> | fields               |  optional | Not Available  | string | Comma-separated list of fields that will be included in the result. So far, only top-level fields in relay or bridge objects of details documents can be specified, e.g., nickname,hashed_fingerprint. If the fields parameter is provided, all other fields which are not contained in the provided list will be removed from the result. Field names are case-insensitive.  |
> | order                |  optional | Not Available  | string | Re-order results by a comma-separated list of fields in ascending or descending order. Results are first ordered by the first list element, then by the second, and so on. Possible fields for ordering are: consensus_weight and first_seen. Field names are case-insensitive. Ascending order is the default; descending order is selected by prepending fields with a minus sign (-). Field names can be listed at most once in either ascending or descending order. Relays or bridges which don't have any value for a field to be ordered by are always appended to the end, regardless or sorting order. The ordering is defined independent of the requested document type and does not require the ordering field to be contained in the document. If no order parameter is given, ordering of results is undefined.  |
> | offset               |  optional | Not Available  | string | Skip the given number of relays and/or bridges. Relays are skipped first, then bridges. Non-positive offset values are treated as zero and don't change the result.  |
> | limit                |  optional | Available  | string | Limit result to the given number of relays and/or bridges. Relays are kept first, then bridges. Non-positive limit values are treated as zero and lead to an empty result. When used together with offset, the offsetting step precedes the limiting step.  |


##### Responses

> | HTTP Code     | Content-Type                      | Response                                                            |
> |---------------|-----------------------------------|---------------------------------------------------------------------|
> | `200`         | `application/json`                | `{...}`                                |
> | `500`         | `text/html;charset=utf-8`         | None                                                                |

##### Example cURL

> ```js
>  curl -X GET -H "Content-Type: application/json" http://localhost:8080/details
> ```

</details>


#### Get relay/bridges badwidth

Bandwidth documents contain aggregate statistics of a relay's or bridge's consumed bandwidth for different time intervals. Bandwidth documents are only updated when a relay or bridge publishes a new server descriptor, which may take up to 18 hours during normal operation.

<details>
 <summary><code>GET</code> <code><b>/bandwidth</b></code> <code>Get relays/bridges bandwidth</code></summary>

##### Parameters

> | Name                 |  Type     | Data type               | Description                      |
> |----------------------|-----------|-------------------------|----------------------------------|
> | type                 |  optional | string | Return only relay (parameter value relay) or only bridge documents (parameter value bridge). Parameter values are case-insensitive.  |
> | lookup               |  optional | string | Return only the relay with the parameter value matching the fingerprint or the bridge with the parameter value matching the hashed fingerprint. Fingerprints should always be hashed using SHA-1, regardless of looking up a relay or a bridge, in order to not accidentally leak non-hashed bridge fingerprints in the URL. Lookups only work for full fingerprints or hashed fingerprints consisting of 40 hex characters. Lookups are case-insensitive.  |


##### Responses

> | HTTP Code     | Content-Type                      | Response                                                            |
> |---------------|-----------------------------------|---------------------------------------------------------------------|
> | `200`         | `application/json`                | `{...}`                                |
> | `500`         | `text/html;charset=utf-8`         | None                                                                |

##### Example cURL

> ```js
>  curl -X GET -H "Content-Type: application/json" http://localhost:8080/bandwidth
> ```

</details>


#### Get relay/bridges weights

Weights documents contain aggregate statistics of a relay's probability to be selected by clients for building paths. Weights documents contain different time intervals and are available for relays only.

<details>
 <summary><code>GET</code> <code><b>/weights</b></code> <code>Get relays/bridges weights</code></summary>

##### Parameters

> | Name                 |  Type     | Data type               | Description                      |
> |----------------------|-----------|-------------------------|----------------------------------|
> | type                 |  optional | string | Return only relay (parameter value relay) or only bridge documents (parameter value bridge). Parameter values are case-insensitive.  |
> | lookup               |  optional | string | Return only the relay with the parameter value matching the fingerprint or the bridge with the parameter value matching the hashed fingerprint. Fingerprints should always be hashed using SHA-1, regardless of looking up a relay or a bridge, in order to not accidentally leak non-hashed bridge fingerprints in the URL. Lookups only work for full fingerprints or hashed fingerprints consisting of 40 hex characters. Lookups are case-insensitive.  |


##### Responses

> | HTTP Code     | Content-Type                      | Response                                                            |
> |---------------|-----------------------------------|---------------------------------------------------------------------|
> | `200`         | `application/json`                | `{...}`                                |
> | `500`         | `text/html;charset=utf-8`         | None                                                                |

##### Example cURL

> ```js
>  curl -X GET -H "Content-Type: application/json" http://localhost:8080/weights
> ```

</details>


#### Get relay/bridges clients

Clients documents contain estimates of the average number of clients connecting to a bridge every day. There are no clients documents available for relays, just for bridges. Clients documents contain different time intervals and are available for bridges only.

<details>
 <summary><code>GET</code> <code><b>/clients</b></code> <code>Get relays/bridges clients</code></summary>

##### Parameters

> | Name                 |  Type     | Data type               | Description                      |
> |----------------------|-----------|-------------------------|----------------------------------|
> | type                 |  optional | string | Return only relay (parameter value relay) or only bridge documents (parameter value bridge). Parameter values are case-insensitive.  |
> | lookup               |  optional | string | Return only the relay with the parameter value matching the fingerprint or the bridge with the parameter value matching the hashed fingerprint. Fingerprints should always be hashed using SHA-1, regardless of looking up a relay or a bridge, in order to not accidentally leak non-hashed bridge fingerprints in the URL. Lookups only work for full fingerprints or hashed fingerprints consisting of 40 hex characters. Lookups are case-insensitive.  |


##### Responses

> | HTTP Code     | Content-Type                      | Response                                                            |
> |---------------|-----------------------------------|---------------------------------------------------------------------|
> | `200`         | `application/json`                | `{...}`                                |
> | `500`         | `text/html;charset=utf-8`         | None                                                                |

##### Example cURL

> ```js
>  curl -X GET -H "Content-Type: application/json" http://localhost:8080/clients
> ```

</details>
