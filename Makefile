.PHONY: database
database: tables init_db load_tables

.PHONY: init_db
init_db:
	./scripts/init_db.sh

tables:
	./scripts/pull_tables.sh

.PHONY: load_tables
load_tables:
	scripts/load_tables.sh

clean:
	rm -rf tables
