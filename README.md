# Network Status APIs

Network Status APIs is a web-based protocol to learn about currently running Tor
relays and bridges.  Network Status APIs itself was not designed as a service
for human beings---at least not directly.

If you want to host your own version of Network Status APIs, read `INSTALL.md`
